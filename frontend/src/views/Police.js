import React, { Component } from 'react'
import { Container } from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import MyNav from '../components/Nav'
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import paginationFactory from 'react-bootstrap-table2-paginator';
import NumberFormatter from '../components/NumberFormatter'

import './Instances.css'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-\
paginator.min.css';


// These are the columns that will be in the table
// When adding more columns, make sure dataField matches the API 
const columns = [{
	dataField: 'name',
	text: 'Department Name',
	sort: true
}, {
	dataField: 'state',
	text: 'State',
	sort: true
}, {
	dataField: 'population',
	text: 'Population',
	sort: true
}, {
	dataField: 'officer_pop',
	text: 'Officers',
	sort: true
}, {
	dataField: 'officer_count_per_1000_civs',
	text: 'Officers/1000 citizens',
	sort: true
}];


class Police extends Component {

	constructor(props) {
		super(props)
		this.state = {
			rows: []
		}

	}

	// Go to the correct police page with the user clicks the row
	tableRowEvents = {
		onClick: (e, row, rowIndex) => {
			this.props.history.push("Police/" + row.id);
		}
	}

	componentDidMount() {
		this.fetchData()
		window.scrollTo(0, 0) // Scroll to the top of the page 
	}

	// make an api call for police data
	fetchData() {
		fetch('https://api.copwatchusa.com/data/departments')
			.then(res => res.json())
			.then((data) => {
				this.setState({ rows: data })
			})
			.catch(console.log)
	}


	// Render the police model page with as a table with search bar
	// and pagination
	render() {
		const { SearchBar } = Search;

		return (
			<div>
				<MyNav active="2" />
				<Container>
					<div className="centered-horizontal">
						<Row>
							<h1>Police Departments</h1>
						</Row>
					</div>

					<Container>
						<ToolkitProvider
							keyField='id'
							data={this.state.rows}
							columns={columns}
							search
						>
							{
								props => (
									<div>
										<SearchBar placeholder="Filter By..."
											{...props.searchProps} />
										<BootstrapTable
											rowEvents={this.tableRowEvents}
											pagination={paginationFactory
												({
													sizePerPageList:
														[{
															text: '10th',
															value: 10
														}]
												})}
											{...props.baseProps}
										/>
									</div>
								)
							}
						</ToolkitProvider>
					</Container>
				</Container>
			</div>
		);
	}
}



export default Police
