import React, { Component } from 'react'
import { Container } from 'react-bootstrap'
import { Card } from 'react-bootstrap'
import { Form } from 'react-bootstrap'
import { Button } from 'react-bootstrap'
import Carousel from 'react-bootstrap/Carousel'
import carousel5 from './../media/images/homepage/carousel5.jpg'
import carousel2 from './../media/images/homepage/carousel2.jpg'
import carousel3 from './../media/images/homepage/carousel3.jpg'
import MyNav from '../components/Nav'
import { useHistory } from 'react-router-dom';


class Homepage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            search: ""
        }
    }

    // render the splash page carousel and the search bar
    render() {
        return (
            <div>
                <MyNav active="1" />
                <Carousel>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src={carousel5}
                            alt="First slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src={carousel2}
                            alt="Third slide"
                        />
                    </Carousel.Item>
                    <Carousel.Item>
                        <img
                            className="d-block w-100"
                            src={carousel3}
                            alt="Third slide"
                        />
                    </Carousel.Item>
                </Carousel>
                <Container className="centered-horizontal">
                    <Card style={{ width: "100vw" }}>
                        <Card.Body>
                            <SearchBox search={this.state.search} />
                        </Card.Body>
                    </Card>
                </Container>
            </div>
        )
    }
}
//Render the search box on the home page, flexible to allow a default search
function SearchBox(props) {
    let s = props.search;
    const history = useHistory();
    return (
        <Form onSubmit={e => {
            history.push("Search/" + s)
        }}>
            <Form.Group controlId="searchBar">
                <Form.Control type="seachQuery"
                    placeholder="Search..."
                    onChange={e => { s = e.target.value; }} />
                <Form.Text className="text-muted">
                    Search for names, cities, and states
                </Form.Text>
            </Form.Group>
            <div className="centered-horizontal">
                <Button
                    variant="primary"
                    type="submit">
                    Search
                </Button>
            </div>
        </Form>
    );
}





export default Homepage
