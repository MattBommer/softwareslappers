import React, { Component } from 'react'
import MyNav from '../components/Nav'
import { Container } from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';
import NumberFormatter from '../components/NumberFormatter'

import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-\
paginator.min.css';
import './Instances.css'


// These are the columns for the table of social services
// When adding more columns, make sure dataField matches the API 
const columns = [{
	dataField: 'type_ss',
	text: 'Service',
	sort: true
}, {
	dataField: 'state',
	text: 'State',
	sort: true
}, {
	dataField: 'dependent_persons',
	text: 'Dependents',
	sort: true
}, {
	dataField: 'avg_funds_per_month',
	text: 'Average Monthly Funding',
	sort: true
}, {
	dataField: 'percent_change_dependent_persons',
	text: 'Growth in Dependents (%)',
	sort: true
}];


class SocialServices extends Component {

	constructor(props) {
		super(props);

		this.state = {
			rows: []
		}
	}
	//Clicking a row will send you to the proper instance page
	tableRowEvents = {
		onClick: (e, row, rowIndex) => {
			this.props.history.push("SocialService/" + row.id);
		}
	}

	componentDidMount() {
		this.fetchData()
		window.scrollTo(0, 0) // Scroll to the top of the page 
	}

	//Api call helper method
	fetchData() {
		fetch('https://api.copwatchusa.com/data/socialservices')
			.then(res => res.json())
			.then((data) => {
				this.setState({ rows: data })
			})
			.catch(console.log)
	}

	// Render the social services model page with a table that has 
	// searching and pagination
	render() {
		const { SearchBar } = Search;
		return (
			<div>
				<MyNav active="4" />
				<Container>
					<div className="centered-horizontal">
						<Row>
							<h1>Social Services</h1>
						</Row>
					</div>


					<Container>
						<ToolkitProvider
							keyField='id'
							data={this.state.rows}
							columns={columns}
							search
						>
							{
								props => (
									<div>
										<SearchBar
											placeholder="Filter By..."
											{...props.searchProps} />
										<BootstrapTable
											rowEvents={this.tableRowEvents}
											pagination={paginationFactory(
												{
													sizePerPageList:
														[{
															text: '10th',
															value: 10
														}]
												})}
											{...props.baseProps}
										/>
									</div>
								)
							}
						</ToolkitProvider>

					</Container>
				</Container>
			</div>
		);
	}
}

export default SocialServices
