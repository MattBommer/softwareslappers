import React, { Component } from 'react'
import { Container, Card, Form, Button } from 'react-bootstrap'
import DemoSearchEntry from '../components/DemoSearchEntry'
import DeptSearchEntry from '../components/DeptSearchEntry'
import ServiceSearchEntry from '../components/ServiceSearchEntry'
import { Link } from 'react-router-dom';



class SearchPage extends Component {

	constructor(props) {
		//send in query prop as the search.
		super(props)
		this.state = {
			query: this.props.query,
			demoResults: [],
			serviceResults: [],
			deptResults: []
		}
	}

	doSearch(query) {
		const q = query.toLowerCase()
		//Perform fetches and populate state, filtering the results
		// by the query
		fetch('https://api.copwatchusa.com/data/departments')
			.then(res => res.json())
			.then((departments) => {
				fetch('https://api.copwatchusa.com/data/socialservices')
					.then(res => res.json())
					.then((services) => {
						fetch('https://api.copwatchusa.com/data/demographics')
							.then(res => res.json())
							.then((demos) => {
								const filteredServices = services.filter
								(serv => {
									return serv.type_ss.toLowerCase()
									.indexOf(q) !== -1 
									|| serv.state.toLowerCase()
									.indexOf(q) !== -1
								})
								const filteredDepts = departments.filter
								(dept => {
									return dept.city.toLowerCase().indexOf(q) 
									!== -1 
									|| dept.state.toLowerCase().indexOf(q) 
									!== -1
								})
								const filteredDemos = demos.filter(demo => {
									return demo.city.toLowerCase().indexOf(q) 
									!== -1 
									|| 
									demo.state.toLowerCase().indexOf(q) !== -1
								})
								this.setState({ demoResults: filteredDemos, 
									serviceResults: filteredServices, 
									deptResults: filteredDepts })
							})
					})
			})
	}

	componentDidMount() {
		this.doSearch(this.state.query)
	}

	// Render the search bar and search results
	render() {
		return (
			<div>
				<Container className="centered-horizontal">
					<Card style={{ width: "100vw" }}>
						<Card.Body>
							<Form onSubmit={ e=> {
								e.preventDefault()
							}}>
								<Form.Group controlId="searchBar">
									<Form.Control 
										type="seachQuery" 
										placeholder="search..." 
										defaultValue={this.state.query} 
										onChange={e => 
										{ this.setState(
											{ query: e.target.value }) }} />
									<Form.Text className="text-muted">
										Search for names, cities, and states
                                    </Form.Text>
								</Form.Group>
								<div className="centered">
									<Button 
										variant="primary" 
										type="submit" 
										onClick={e => 
											{this.doSearch(this.state.query) }}
											as={Link} 
											to={"/Search/" + this.state.query}>
										Search
                                </Button>
								</div>
							</Form>
						</Card.Body>
					</Card>
				</Container>

				{ this.state.demoResults.map(res => 
					<DemoSearchEntry 
						key={res.id} 
						res={res} 
						query={this.state.query} />)}
				{ this.state.serviceResults.map(res => 
					<ServiceSearchEntry 
						key={res.id} 
						res={res} 
						query={this.state.query} />)}
				{ this.state.deptResults.map(res => 
					<DeptSearchEntry 
						key={res.id} 
						res={res} 
						query={this.state.query} />)}

			</div>
		);
	}

}

export default SearchPage