import React, { Component } from 'react'
import { Container, Tabs, Tab } from 'react-bootstrap'
import MyNav from '../components/Nav'
import SSBarChart from '../components/BarChart'
import DepPieChart from '../components/PieChart'
import RaceRadarChart from '../components/RadarChart'
import MedtroBarChart from '../components/MedtroBarChart'
import MedtroPieChart from '../components/MedtroPieChart'
import MedtroLineChart from '../components/MedtroLineChart'


class Visuals extends Component {

	constructor(props) {
		super(props);

		this.state = {
			social: [],
			demo: [],
			department: [],
			doctors: [],
			vis1: [],
			vis2: [],
			vis31: [],
			vis32: [],
			vis4: [],
			vis5: [],
			vis6: []
		}
	}

	componentDidMount() {
		this.fetchSocialServiceData()
		this.fetchDemographicDepartmentData()
		this.fetchDoctorData()
		this.fetchAuxServicesData()

		window.scrollTo(0, 0) // Scroll to the top of the page
	}

	//Api call helper method
	fetchSocialServiceData() {
		fetch('https://api.copwatchusa.com/data/socialservices')
			.then(res => res.json())
			.then((data) => {
				let visData = data.map((dict) => {
					return {
						"state": dict.state + ` (${dict.type_ss})`,
						"households": dict.dependent_households,
						"dependents": dict.dependent_persons
					}
				})
				this.setState({ vis1: visData, social: data })
			})
			.catch(console.log)
	}

	fetchDemographicDepartmentData() {
		fetch('https://api.copwatchusa.com/data/demographics')
			.then(demoResponse => demoResponse.json())
			.then((demoData) => {
				fetch('https://api.copwatchusa.com/data/departments')
					.then(departmentResponse => departmentResponse.json())
					.then((depData) => {

						const keys = ["Black", "White", "Native American", "Asian", "Pacific Islander", "Other"]
						var vis2Data = keys.map((key) => {
							return {
								"race": key,
								"demographics": 0,
								"offender": 0,
								"fullMark": 100
							}
						})

						var vis31Data = [{
							"name": "national female",
							"value": 0
						},
						{
							"name": "national male",
							"value": 0
						}]

						var vis32Data = [{
							"name": "police female",
							"value": 0
						},
						{
							"name": "police male",
							"value": 0
						}]



						for (var city in demoData) {
							vis31Data[0].value += Math.round(demoData[city].female / demoData[city].population * 100)
							vis31Data[1].value += Math.round(demoData[city].male / demoData[city].population * 100)

							vis2Data[0].demographics += Math.round(demoData[city].black / demoData[city].population * 100)
							vis2Data[1].demographics += Math.round(demoData[city].white / demoData[city].population * 100)
							vis2Data[2].demographics += Math.round(demoData[city].american_indian / demoData[city].population * 100)
							vis2Data[3].demographics += Math.round(demoData[city].asian / demoData[city].population * 100)
							vis2Data[4].demographics += Math.round(demoData[city].native_hawaiian / demoData[city].population * 100)
							vis2Data[5].demographics += Math.round(demoData[city].other / demoData[city].population * 100)
						}

						for (var dep in depData) {
							vis32Data[0].value += depData[dep].female
							vis32Data[1].value += depData[dep].male

							vis2Data[0].offender += depData[dep].black
							vis2Data[1].offender += depData[dep].white
							vis2Data[2].offender += depData[dep].american_indian
							vis2Data[3].offender += depData[dep].asian
							vis2Data[4].offender += depData[dep].native_hawaiian
							vis2Data[5].offender += depData[dep].unknown
						}

						for (var i = 0; i < vis2Data.length; i++) {
							vis2Data[i].offender /= 100
							vis2Data[i].demographics /= 100
						}

						for (var j = 0; j < vis31Data.length; j++) {
							vis31Data[j].value /= 100
						}

						this.setState({ department: depData, demo: demoData, vis2: vis2Data, vis31: vis31Data, vis32: vis32Data })
					})
					.catch(console.log)
			})
			.catch(console.log)
	}

	fetchDoctorData() {
		fetch('https://www.mymedtro.com/api/doctors?results_per_page=100')
			.then(res => res.json())
			.then((data) => {
				fetch('https://www.mymedtro.com/api/doctors?results_per_page=100page=2')
					.then(res2 => res2.json())
					.then((data2) => {

						// count the doctors in each city and gender from page 1 of the data
						let cities = {}
						let genders = {
							"Male": 0,
							"Female": 0
						}
						for (let index in data.objects) {
							let doctor = data.objects[index]
							if (doctor.city in cities) {
								cities[doctor.city] += 1
							} else {
								cities[doctor.city] = 1
							}
							if (doctor.gender === "M") {
								genders["Male"] += 1
							} else if (doctor.gender === "F") {
								genders["Female"] += 1
							}
						}

						// count doctors in each city and gender from page 2 of data
						for (let index in data2.objects) {
							let doctor = data2.objects[index]
							if (doctor.city in cities) {
								cities[doctor.city] += 1
							} else {
								cities[doctor.city] = 1
							}
							if (doctor.gender === "M") {
								genders["Male"] += 1
							} else if (doctor.gender === "F") {
								genders["Female"] += 1
							}
						}

						// Put city data in correct format for recharts
						let vis4Data = []
						for (let cityName in cities) {
							let numDocs = cities[cityName]
							let cityData = { "city": cityName, "doctors": numDocs }
							vis4Data.push(cityData)
						}

						// Put gender data in correct format for recharts
						let vis5Data = [
							{ "name": "Female", "value": genders["Female"] },
							{ "name": "Male", "value": genders["Male"] }
						]

						this.setState({ doctors: data.objects, vis4: vis4Data, vis5: vis5Data })
					})
			})
			.catch(console.log)
	}

	fetchAuxServicesData() {
		fetch('https://www.mymedtro.com/api/aux_services?results_per_page=200')
			.then(res => res.json())
			.then((data) => {
				let service_cities = {}

				for (let index in data.objects) {
					let service = data.objects[index]
					if (service.city in service_cities) {
						service_cities[service.city] += 1
					}
					else {
						service_cities[service.city] = 1
					}
				}

				let vis6Data = []
				for (let cityName in service_cities) {
					let numServ = service_cities[cityName]
					let cityData = { "city": cityName, "services": numServ }
					vis6Data.push(cityData)
				}

				this.setState({ vis6: vis6Data })
			})
			.catch(console.log)
	}

	render() {
		return (
			<div>
				<MyNav active="6" />
				<div className="padding-vertical">
					<Tabs defaultActiveKey="Us">
						{/* Our group's visualizations */}
						<Tab eventKey="Us" title="Our Visualizations">
							<Container>
								<div className="padding-vertical">
									<h1>Our Visualizations</h1>
								</div>
								<DepPieChart data1={this.state.vis31} data2={this.state.vis32} title={"Proportion of Men to Women in Police Compared to National"} />
								<SSBarChart data={this.state.vis1} title={"Number of Social Service Dependents by State"} />
								<RaceRadarChart data={this.state.vis2} title={"Racial Composition of Detainees as it Relates to Composite City Demographics"} />
							</Container>
						</Tab>
						{/* Visualizations for MyMedtro */}
						<Tab eventKey="Provider" title="MyMedtro's Visualizations">
							<Container>
								<div className="padding-vertical">
									<h1>MyMedtro's Visualizations</h1>
								</div>
								<MedtroBarChart data={this.state.vis4} title="Number of Doctors in Texas Cities Available on MyMedtro" />
								<MedtroPieChart data={this.state.vis5} title="Ratio of Male to Female Doctors on MyMedtro" />
								<MedtroLineChart data={this.state.vis6} title="Number of auxillary services by city on MyMedtro" />
							</Container>
						</Tab>
					</Tabs>
				</div>
			</div >
		);
	}
}

export default Visuals;