import React, { Component } from 'react'
import { Row, Card, Container } from 'react-bootstrap'
import MyNav from '../components/Nav'
import './Instances.css'

// Import the images used in our bios and tools
import kim from '../media/images/about/kim.jpg'
import matt from '../media/images/about/matt.jpg'
import michael from '../media/images/about/michael.jpg'
import jarod from '../media/images/about/jarod.jpg'
import gitlab from '../media/images/about/gitlab-icon.png'
import postman from '../media/images/about/postman-icon.png'
import react from '../media/images/about/react-icon.png'
import reactstrap from '../media/images/about/reactbootstrap-icon.png'
import material from '../media/images/about/material-icon.png'
import aws from '../media/images/about/aws-icon.png'
import yarn from '../media/images/about/yarn-icon.png'
import jest from '../media/images/about/jest-icon.png'
import enzyme from '../media/images/about/enzyme-icon.png'
import buddy from '../media/images/about/buddy-icon.png'
import flask from '../media/images/about/flask-icon.png'
import postgresql from '../media/images/about/postgresql-icon.png'
import sqlalchemy from '../media/images/about/sqlalchemy-icon.png'
import flaskrestful from '../media/images/about/flaskrestful-icon.png'
import flaskrestless from '../media/images/about/flaskrestless-icon.png'
import marshmallow from '../media/images/about/marshmallow-icon.png'
import waitress from '../media/images/about/waitress-icon.png'
import unittest from '../media/images/about/unittest-icon.png'
import selenium from '../media/images/about/selenium-icon.png'
import table from '../media/images/about/table-icon.png'


class About extends Component {

    constructor() {
        super()
        this.state = {
            commits: {},
            issues: {},
            unittests: {}
        };
    }

    componentDidMount() {
        // Make a GET request for commit data
        fetch("https://gitlab.com/api/v4/projects/" +
            "21271730/repository/contributors")
            // Parse output to JSON
            .then(commits => commits.json())
            // Set the value of our state to output from API call
            .then((commitData) => {
                // Do it again with issue data
                fetch("https://gitlab.com/api/v4/projects/" +
                    "21271730/issues?per_page=200")
                    .then(issues => issues.json())
                    .then((issueData) => {
                        fetch("https://api.copwatchusa.com/data/unittests")
                            .then(unit => unit.json())
                            .then((unittest_json) => {
                                var name = ""
                                var unittests = {}
                                unittests = unittest_json.objects[0]
                                // Save everyone's commits
                                var commits = {}
                                var commitTotal = 0
                                for (let data in commitData) {
                                    name = commitData[data].name
                                    var count = commitData[data].commits
                                    commits[name] = count
                                    commitTotal += count
                                }
                                commits["Total"] = commitTotal

                                // Save everyone's issues
                                var issues = {}
                                var issueTotal = 0
                                for (let data in issueData) {
                                    var assignees = issueData[data].assignees
                                    for (let person in assignees) {
                                        name = assignees[person].name
                                        if (name in issues) {
                                            issues[name] += 1
                                        } else {
                                            issues[name] = 1
                                        }
                                    }
                                    issueTotal += 1

                                }
                                issues["Total"] = issueTotal

                                this.setState({
                                    commits: commits,
                                    issues: issues, unittests: unittests
                                })
                            })
                    })
            })
            // Log errors to console
            .catch(console.log)
    }

    render() {
        return (
            <div>
                <MyNav active="5" />
                <div className="padding-vertical">
                    <Container>
                        <h1>About CopWatchUSA</h1>
                        <h6>
                            During these politically tumultuous times, we
                            aim to track police efficacy throughout the US in
                            relation to population demographics and discover
                            whether defunding the police and diverting those
                            funds to social welfare programs would improve
                            communities and decrease incarceration rates. We do
                            not claim to speak for those affected by racial
                            bias in policing. This website is intended for
                            those like us, who are interested in gaining a
                            better understanding of the relationship between
                            social class and incarceration rates through
                            exploration of statistics.
                        </h6>
                    </Container>
                </div>
                <Container>
                    <div className="centered-horizontal">
                        <Row>
                            <div className="padding-horizontal">
                                <Card style=
                                    {{ width: '22rem', height: '700px' }}>
                                    <Card.Img variant="top" src={matt} />
                                    <Card.Body>
                                        <a href="https://www.linkedin.com/in/mattbommer/">
                                            <Card.Title>Matt Bommer</Card.Title>
                                        </a>
                                        <Card.Subtitle>Team Lead and Full Stack</Card.Subtitle>
                                        <Card.Text>
                                            I am a Senior CS student here at UT Austin. I was born and raised in a small city
                                            in southeast Texas, Beaumont. In my free time I enjoy skating, playing video games,
                                            and building random python applications!
                                    </Card.Text>
                                        <Card.Text><b>Commits:</b> {this.state.commits["Matt Bommer"]}</Card.Text>
                                        <Card.Text><b>Issues:</b> {this.state.issues["Matt Bommer"]}</Card.Text>
                                        <Card.Text><b>Unit Tests:</b> {this.state.unittests["matt_unit"]}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div className="padding-horizontal">
                                <Card style={{ width: '22rem', height: '700px' }}>
                                    <Card.Img variant="top" src={jarod} />
                                    <Card.Body>
                                        <Card.Title>Jarod Norwood</Card.Title>
                                        <Card.Subtitle>Full Stack</Card.Subtitle>
                                        <Card.Text>
                                            I am a Junior CS and Philosophy student at UT.
                                            I grew up north of Dallas. I spend my free time playing games and reading.
                                            I want to create a quality data source to paint an accurate picture of
                                            policing in our communities.
                                        </Card.Text>
                                        <Card.Text><b>Commits:</b> {this.state.commits["Jarod"]}</Card.Text>
                                        <Card.Text><b>Issues:</b> {this.state.issues["Jarod C Norwood"]}</Card.Text>
                                        <Card.Text><b>Unit Tests:</b>  {this.state.unittests["jarod_unit"]}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        </Row>
                    </div>
                    <div className="centered-horizontal">
                        <Row>
                            <div className="padding-horizontal">
                                <Card style={{ width: '22rem', height: '700px' }}>
                                    <Card.Img variant="top" src={michael} />
                                    <Card.Body>
                                        <a href="https://www.linkedin.com/in/michaelkajihara/">
                                            <Card.Title>Michael Kajihara</Card.Title>
                                        </a>
                                        <Card.Subtitle>Back End</Card.Subtitle>
                                        <Card.Text>
                                            I am a Junior CS student at UT Austin.
                                            I grew up just North of Dallas, Texas, and outside of schoolwork I spend
                                            time competing in CTF's, kayaking, and crocheting amigurumi animals.
                                        </Card.Text>
                                        <Card.Text><b>Commits:</b> {this.state.commits["Michael Kajihara"]}</Card.Text>
                                        <Card.Text><b>Issues:</b> {this.state.issues["Michael Kajihara"]}</Card.Text>
                                        <Card.Text><b>Unit Tests:</b> {this.state.unittests["michael_unit"]}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div className="padding-horizontal">
                                <Card style={{ width: '22rem', height: '700px' }}>
                                <Card.Img variant="top" src={kim} />
                                    <Card.Body>
                                        <a href="https://www.linkedin.com/in/kim-ha-8a9502199/">
                                            <Card.Title>Kim Ha</Card.Title>
                                        </a>
                                        <Card.Subtitle>Front End</Card.Subtitle>
                                        <Card.Text>
                                            Hi! I'm a Senior UTCS student. I'm proud to work on this project because
                                            it promotes a topic I really care about. Outside of CS, I like to cook
                                            fancy meals and read about religions.
                                        </Card.Text>
                                        <Card.Text><b>Commits:</b> {this.state.commits["kim-ha"]}</Card.Text>
                                        <Card.Text><b>Issues:</b> {this.state.issues["Kim Ha"]}</Card.Text>
                                        <Card.Text><b>Unit Tests:</b> {this.state.unittests["kim_unit"]}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        </Row>
                    </div>
                </Container>
                <Container>
                    <div className="centered-horizontal">
                        <Row>
                            <div className="padding-horizontal">
                                <Card style={{ height: '100%' }}>
                                    <Card.Body>
                                        <Card.Title>Our Links:</Card.Title>
                                        <Card.Text>
                                            <a href="https://gitlab.com/MattBommer/softwareslappers/" target="_blank" rel="noopener noreferrer">
                                                GitLab Repo
                                                    </a>
                                        </Card.Text>
                                        <Card.Text>
                                            <a href="https://documenter.getpostman.com/view/12813036/TVYNYEpx" target="_blank" rel="noopener noreferrer">
                                                Postman API
                                                    </a>
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                            <div className="padding-horizontal">
                                <Card>
                                    <Card.Body>
                                        <Card.Title>Website Statistics</Card.Title>
                                        <Card.Text><b>Commits:</b> {this.state.commits["Total"]}</Card.Text>
                                        <Card.Text><b>Issues:</b> {this.state.issues["Total"]}</Card.Text>
                                        <Card.Text><b>Unit Tests:</b> {this.state.unittests["total"]}</Card.Text>
                                    </Card.Body>
                                </Card>
                            </div>
                        </Row>
                    </div>
                </Container>
                <Container>
                    <div className="centered-horizontal">
                        <Card>
                            <Card.Body>
                                <Card.Title>Tools:</Card.Title>
                                <div className="centered-horizontal">
                                    <Row>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://gitlab.com/'>
                                                    <Card.Img variant="top" className="icon" src={gitlab} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>GitLab</Card.Title>
                                                    <Card.Text>Git repository manager used to store our code and share access with team mates.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://www.postman.com/'>
                                                    <Card.Img variant="top" className="icon" src={postman} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Postman</Card.Title>
                                                    <Card.Text>API creation tool for making and designing APIs.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://reactjs.org/'>
                                                    <Card.Img variant="top" className="icon" src={react} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>React</Card.Title>
                                                    <Card.Text>Javascript library used to make our website responsive.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://react-bootstrap.github.io/'>
                                                    <Card.Img variant="top" className="icon" src={reactstrap} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title >React Bootstrap</Card.Title>
                                                    <Card.Text>React UI framework that makes our website and its components pretty.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </Row>
                                </div>
                                <div className="centered-horizontal">
                                    <Row>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://jestjs.io/docs/en/tutorial-react'>
                                                    <Card.Img variant="top" className="icon" src={jest} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Jest</Card.Title>
                                                    <Card.Text>Javascript package manager used to stream line development of application and manage project dependencies. </Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://enzymejs.github.io/enzyme/'>
                                                    <Card.Img variant="top" className="icon" src={enzyme} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Enzyme</Card.Title>
                                                    <Card.Text>Javascript testing framework.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://buddy.works/docs/pipelines'>
                                                    <Card.Img variant="top" className="icon" src={buddy} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Buddy</Card.Title>
                                                    <Card.Text>An extremly simple and easy to use automation platform for all your CI needs.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://flask.palletsprojects.com/en/1.1.x/'>
                                                    <Card.Img variant="top" className="icon" src={flask} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Flask</Card.Title>
                                                    <Card.Text>Micro web framework for the backend server.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </Row>
                                </div>
                                <div className="centered-horizontal">
                                    <Row>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://www.postgresql.org/'>
                                                    <Card.Img variant="top" className="icon" src={postgresql} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>PostgreSQL</Card.Title>
                                                    <Card.Text>The relational database system we use to store our data.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://flask-sqlalchemy.palletsprojects.com/en/2.x/'>
                                                    <Card.Img variant="top" className="icon" src={sqlalchemy} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>SQLAlchemy</Card.Title>
                                                    <Card.Text>SQL toolkit that gives us an ORM to interact with the database.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://flask-restful.readthedocs.io/en/latest/'>
                                                    <Card.Img variant="top" className="icon" src={flaskrestful} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Flask Restful</Card.Title>
                                                    <Card.Text>Flask extension that provides simple generation of APIs.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://flask-restless.readthedocs.io/en/stable/'>
                                                    <Card.Img variant="top" className="icon" src={flaskrestless} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Flask Restless</Card.Title>
                                                    <Card.Text>Flask extension that provides a framework for creating APIs.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </Row>
                                </div>
                                <div className="centered-horizontal">
                                    <Row>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://marshmallow.readthedocs.io/en/stable/index.html'>
                                                    <Card.Img variant="top" className="icon" src={marshmallow} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Marshmallow</Card.Title>
                                                    <Card.Text>Python library used to convert SQLAlchemy Models into JSON using schemas.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://docs.python.org/3/library/unittest.html'>
                                                    <Card.Img variant="top" className="icon" src={waitress} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Waitress</Card.Title>
                                                    <Card.Text>Production python WSGI server used to serve our backend.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://docs.python.org/3/library/unittest.html'>
                                                    <Card.Img variant="top" className="icon" src={unittest} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Python Unittest</Card.Title>
                                                    <Card.Text>Python testing framework</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://www.selenium.dev/'>
                                                    <Card.Img variant="top" className="icon" src={selenium} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Selenium</Card.Title>
                                                    <Card.Text>Webdriver used to automate the scraping of images and for the testing of the UI.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </Row>
                                </div>
                                <div className="centered-horizontal">
                                    <Row>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://material-ui.com/'>
                                                    <Card.Img variant="top" className="icon" src={material} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Material UI</Card.Title>
                                                    <Card.Text>React UI framework for building specific web components that make our site easier to use.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://aws.amazon.com/'>
                                                    <Card.Img variant="top" className="icon" src={aws} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>AWS</Card.Title>
                                                    <Card.Text>On demand cloud-platform used for hosting our static website.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://yarnpkg.com/package/react'>
                                                    <Card.Img variant="top" className="icon" src={yarn} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>Yarn</Card.Title>
                                                    <Card.Text>Javascript package manager used to stream line development of application and manage project dependencies.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                        <div className="padding-horizontal">
                                            <Card style={{ width: '10rem', height: '400px' }}>
                                                <a href='https://https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/getting-started.html.com/package/react'>
                                                    <Card.Img variant="top" className="icon" src={table} />
                                                </a>
                                                <Card.Body className="description">
                                                    <Card.Title>React Bootstrap Table</Card.Title>
                                                    <Card.Text>A modified version of the React Bootstrap Table that has pagination and searching.</Card.Text>
                                                </Card.Body>
                                            </Card>
                                        </div>
                                    </Row>
                                </div>
                            </Card.Body>
                        </Card>
                    </div>
                </Container>
                <Container>
                    <div className="centered-horizontal">
                        <Card>
                            <Card.Body>
                                <Card.Title>Data Sources: </Card.Title>
                                <Card.Text>
                                    <b><a href="https://crime-data-explorer.fr.cloud.gov/api" target="_blank" rel="noopener noreferrer"> FBI Crime Data Explorer: </a></b>
                                    Used to collect information on general statistics of police departments such as: population served, number of officers,
                                    gender ratio, number of officers per 1000 citizens. Also used to collect data on specific departments such as: number of offenses
                                    (for the year 2019) and the racial composition of offenders.
                                </Card.Text>
                                <Card.Text>
                                    <b><a href="https://www.census.gov/data/developers/data-sets/acs-1year.html" target="_blank" rel="noopener noreferrer"> United States Census: </a></b>
                                    Used to populate our demographics model. This api provided the most recent statistics of general
                                    populations in a specific city, thus making it a crucial source, rich in data about citizens living in specific
                                    cities and giving detailed statistics like: racial composition of city, poverty rates, median age, median household income,
                                    average number of people in a household, poverty rates, percent of uninsured citizens (healthcare).
                                </Card.Text>
                                <Card.Text>
                                    <b><a href="https://www.fns.usda.gov/pd/supplemental-nutrition-assistance-program-snap" target="_blank" rel="noopener noreferrer"> Food and Nutrition Service: </a></b>
                                    Web page that contains excel files with statisical data (by state and nation) about the costs and
                                    number of dependents for the Supplemental Nutrition Assistance Program. Our team parsed through these files and
                                    extracted/extropolated data from the provided statistics to add attributes/data to our social services model.
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </div>
                </Container>
            </div>
        )
    }
}

export default About
