import React, { Component } from 'react'
import { DropdownButton, InputGroup, FormControl } from 'react-bootstrap'
import { Dropdown } from 'react-bootstrap'
import MyNav from '../components/Nav'
import './Instances.css'

import DemoCard from '../components/DemoCard'
import Pagination from '../components/Pagination'

// The code for this pagination comes from this website: 
// https://www.digitalocean.com/



class Demographics extends Component {

	state = {
		demographics: [],
		currentDemos: [],
		currentPage: null,
		totalPages: null,
		query: "",
	}

	componentDidMount() {
		this.fetchData()
	}

	// Fetch the regular, unsorted data
	fetchData() {
		fetch('https://api.copwatchusa.com/data/demographics')
			.then(res => res.json())
			.then((data) => {
				this.setState({ demographics: data })
			})
			.catch(console.log)
	}

	// Update the demographics that are displayed
	onPageChanged = data => {
		const { demographics } = this.state;
		const { currentPage, totalPages, pageLimit } = data;
		const offset = (currentPage - 1) * pageLimit;
		const currentDemos = demographics.slice(offset, offset + pageLimit);

		this.setState({ currentPage, currentDemos, totalPages });
	}

	// Called when the user changes the sort by option
	onSortChange = e => {
		this.updateDemo(e.target.text);
	}

	// Update the grids to display the new sorted demos
	updateGrid(sorted) {
		const pageLimit = 20
		const offset = (this.state.currentPage - 1) * pageLimit;
		const newDemos = sorted.slice(offset, offset + pageLimit);
		this.setState({ demographics: sorted, currentDemos: newDemos })
	}

	// Make a new api call for demographics based on the sort by option chosen
	// and filter by the query
	updateDemo(option) {
		var sorted = []
		if (option === "Default") {
			fetch('https://api.copwatchusa.com/data/demographics')
				.then(res => res.json())
				.then((data) => {
					if (this.state.query !== "") {
						const q = this.state.query.toLowerCase()
						data = data.filter(demo => {
							return demo.city.toLowerCase().indexOf(q) !== -1
								|| demo.state.toLowerCase().indexOf(q) !== -1
						})
					}
					this.updateGrid(data)
				})
		} else if (option === "City") {
			fetch('https://api.copwatchusa.com/data/demographics')
				.then(res => res.json())
				.then((data) => {
					if (this.state.query !== "") {
						const q = this.state.query.toLowerCase()
						data = data.filter(demo => {
							return demo.city.toLowerCase().indexOf(q) !== -1
								|| demo.state.toLowerCase().indexOf(q) !== -1
						})
					}
					sorted = data.sort((a, b) => (a.city > b.city) ? 1 : -1)
					this.updateGrid(sorted)
				})
				.catch(console.log)
		}
		else if (option === "State") {
			fetch('https://api.copwatchusa.com/data/demographics')
				.then(res => res.json())
				.then((data) => {
					if (this.state.query !== "") {
						const q = this.state.query.toLowerCase()
						data = data.filter(demo => {
							return demo.city.toLowerCase().indexOf(q) !== -1
								|| demo.state.toLowerCase().indexOf(q) !== -1
						})
					}
					sorted = data.sort((a, b) => (a.state > b.state) ? 1 : -1)
					this.updateGrid(sorted)
				})
				.catch(console.log)
		}
		else if (option === "Population") {
			fetch('https://api.copwatchusa.com/data/demographics')
				.then(res => res.json())
				.then((data) => {
					if (this.state.query !== "") {
						const q = this.state.query.toLowerCase()
						data = data.filter(demo => {
							return demo.city.toLowerCase().indexOf(q) !== -1
								|| demo.state.toLowerCase().indexOf(q) !== -1
						})
					}
					sorted = data.sort((a, b) => (a.population < b.population)
						? 1 : -1)
					this.updateGrid(sorted)
				})
				.catch(console.log)
		} else if (option === "Poverty") {
			fetch('https://api.copwatchusa.com/data/demographics')
				.then(res => res.json())
				.then((data) => {
					if (this.state.query !== "") {
						const q = this.state.query.toLowerCase()
						data = data.filter(demo => {
							return demo.city.toLowerCase().indexOf(q) !== -1
								|| demo.state.toLowerCase().indexOf(q) !== -1
						})
					}
					sorted = data.sort((a, b) =>
						(a.poverty_rate < b.poverty_rate) ? 1 : -1)
					this.updateGrid(sorted)
				})
				.catch(console.log)
		} else if (option === "Unemployment") {
			fetch('https://api.copwatchusa.com/data/demographics')
				.then(res => res.json())
				.then((data) => {
					if (this.state.query !== "") {
						const q = this.state.query.toLowerCase()
						data = data.filter(demo => {
							return demo.city.toLowerCase().indexOf(q) !== -1
								|| demo.state.toLowerCase().indexOf(q) !== -1
						})
					}
					sorted = data.sort((a, b) =>
						(a.unemployment_rate < b.unemployment_rate) ? 1 : -1)
					this.updateGrid(sorted)
				})
				.catch(console.log)
		}
	}

	// Make a new api call for demographics, filtering by the query that the
	// has typed
	filter(query) {
		var filtered = []
		var q = query.toLowerCase()
		fetch('https://api.copwatchusa.com/data/demographics')
			.then(res => res.json())
			.then((data) => {
				filtered = data.filter(demo => {
					return demo.city.toLowerCase().indexOf(q) !== -1
						|| demo.state.toLowerCase().indexOf(q) !== -1
				})
				if (filtered.length > 0) {
					const pageLimit = 20
					const offset = 0;
					const newDemos = filtered.slice
						(offset, offset + pageLimit);
					this.setState({
						query: query, demographics: filtered,
						currentDemos: newDemos, currentPage: 1
					})
				}
			})
	}

	// Render the grid with demographic models
	render() {
		const { demographics, currentDemos, currentPage, totalPages }
			= this.state;
		const totalDemos = demographics.length;
		if (totalDemos === 0) return null;

		const headerClass = ['text-dark py-2 pr-4 m-0', currentPage ?
			'border-gray border-right' : ''].join(' ').trim();
		return (
			<div>
				<MyNav active="3" />
				<div className="container mb-5">
					<div className="row d-flex flex-row py-5">
						<div className="w-100 px-4 py-5 d-flex flex-row 
						flex-wrap align-items-center justify-content-between">
							<div className="d-flex flex-row align-items-center">
								<h2 className={headerClass}>
									<strong className="text-secondary">
										{totalDemos}</strong> Cities
								</h2>
								{currentPage && (
									<span className="current-page 
									d-inline-block h-100 pl-4 text-secondary">
										Page <span className=
											"font-weight-bold">{currentPage}</span>
										 / <span className="font-weight-bold">
											{totalPages}</span>
									</span>
								)}
							</div>
							<div>
								<InputGroup size="sm" className="mb-3">
									<FormControl
										placeholder="Filter By..."
										onChange={e => this.filter
											(e.target.value)}
									/>
								</InputGroup>
							</div>
							<div className="d-flex flex-row py-4 
								align-items-center">
								<Pagination
									totalRecords={totalDemos}
									pageLimit={20}
									pageNeighbours={1}
									onPageChanged={this.onPageChanged} />
								<DropdownButton
									id="dropdown-basic-button"
									title="Sort By">
									<Dropdown.Item
										onClick={this.onSortChange}>
										Default
									</Dropdown.Item>
									<Dropdown.Item
										onClick={this.onSortChange}>
										State
									</Dropdown.Item>
									<Dropdown.Item
										onClick={this.onSortChange}>
										City
									</Dropdown.Item>
									<Dropdown.Item
										onClick={this.onSortChange}>
										Population
									</Dropdown.Item>
									<Dropdown.Item
										onClick={this.onSortChange}>
										Poverty
									</Dropdown.Item>
									<Dropdown.Item
										onClick={this.onSortChange}>
										Unemployment
									</Dropdown.Item>
								</DropdownButton>
							</div>
						</div>
						{currentDemos.map(city =>
							<DemoCard key={city.id} city={city} />)}
					</div>
				</div>
			</div>
		);
	}

}


export default Demographics
