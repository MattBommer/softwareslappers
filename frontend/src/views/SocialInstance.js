import React, { Component } from 'react'
import { Jumbotron } from 'react-bootstrap'
import { Card } from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import { Link } from "react-router-dom"
import NumberFormatter from '../components/NumberFormatter'
import './Instances.css'


class SocialInstance extends Component {

    constructor(props) {
        super(props)
        this.state = {
            id: this.props.id,
            state: 'N/A',
            avg_funds_per_month: 'N/A',
            avg_funds_per_year: 'N/A',
            avg_monthly_benefits_per_person: 'N/A',
            avg_monthly_benefits_per_household: 'N/A',
            dependent_households: 'N/A',
            dependent_persons: 'N/A',
            media: 'N/A',
            type_ss: 'N/A',
            percent_change_dependent_households: 'N/A',
            percent_change_dependent_persons: 'N/A',
            percent_change_funds_year: 'N/A',
            departments: [],
            cities: []
        }
    }

    // Fetch the appropriate social service and the police departments and
    // demographics that are in the same state
    componentDidMount() {
        fetch('https://api.copwatchusa.com/data/socialservices/'
            + this.state.id)
            .then(response => response.json())
            .then((response) => {
                fetch('https://api.copwatchusa.com/data/departments?state='
                    + response.state)
                    .then(departments => departments.json())
                    .then(departments => {
                        fetch('https://api.copwatchusa.com/data/demographics?state='
                            + response.state)
                            .then(city_res => city_res.json())
                            .then(city_res => {
                                response.departments = departments;
                                response.cities = city_res;
                                this.setState(response);

                            },
                                (error) => {
                                    this.setState({
                                        error
                                    });
                                });
                    });
            });

        window.scrollTo(0, 0) // Scroll to the top of the page 
    }

    // Render the social service instance page
    render() {

        return (

            <div>
                {/* Name, state, population, and link */}
                <Jumbotron>
                    <h1>{this.state.type_ss}</h1>
                    <h3>{this.state.state}</h3>
                    <h3>Dependents: {NumberFormatter(this.state.dependent_persons)}</h3>
                </Jumbotron>

                {/* Usage, Benefits, and Participants */}
                <div className="centered-horizontal">
                    <Row>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>Percentage Change:</Card.Title>
                                <Card.Text>
                                    {this.state.percent_change_dependent_persons}%
                                 change in dependents
                            </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>Average Monthly Benefits:</Card.Title>
                                <Card.Text> {"$ "}
                                    {NumberFormatter(this.state.avg_monthly_benefits_per_household)}
                                    {" "} per Household
                            </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>Participating Households:</Card.Title>
                                <Card.Text>
                                    {NumberFormatter(this.state.dependent_households)}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Row>
                </div>

                {/* Image and more stats */}
                <div className="centered-horizontal">
                    <Row>
                        <div className="centered">
                            <Card style={{ width: '22vw' }}>
                                <Card.Img variant="top" src={this.state.media} />
                            </Card>
                        </div>
                        <div className="centered">
                            <Col>
                                <Card style={{ width: '18vw' }}>
                                    <Card.Body>
                                        <Card.Title>
                                            Annual Funding:
                                        </Card.Title>
                                        <Card.Text>
                                            {"$ "}
                                            {NumberFormatter(this.state.avg_funds_per_year)}
                                        </Card.Text>
                                        <Card.Text>
                                            {this.state.percent_change_funds_year}%
                                            more than last year.
                                        </Card.Text>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </div>
                    </Row>
                </div>

                {/* Links to other models */}
                <div className="centered-horizontal">
                    <Row>
                        <Col>
                            <Card style={{ width: '25rem' }}>
                                <Card.Body>
                                    <Card.Title>
                                        Cities in {this.state.state}
                                    </Card.Title>
                                    <ListCities list={this.state.cities} />
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{ width: '25rem' }}>
                                <Card.Body>
                                    <Card.Title>
                                        Police Departments in
                                        {" "}{this.state.state}
                                    </Card.Title>
                                    <ListDepartments list=
                                        {this.state.departments} />
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}

// List related police departments
function ListDepartments(props) {
    const list = props.list.map((pol) =>
        <div key={pol.id}>
            <Link to={"/Police/" + pol.id}>{pol.name}</Link>
        </div>
    );
    return (
        <div>{list}</div>
    );
}

// List related cities
function ListCities(props) {
    const list = props.list.map((city) =>
        <div key={city.id}>
            <Link to={"/Demographic/" + city.id}>{city.city}</Link>
        </div>
    );
    return (
        <div>{list}</div>
    );
}

export default SocialInstance;