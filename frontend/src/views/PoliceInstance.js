import React, { Component } from 'react'
import { Jumbotron } from 'react-bootstrap'
import { Card } from 'react-bootstrap'
import { Link } from "react-router-dom";
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import NumberFormatter from '../components/NumberFormatter'
import './Instances.css'



class PoliceInstance extends Component {

    constructor(props) {
        super(props)
        this.state = {
            american_indian: 'N/A',
            asian: 'N/A',
            avg_officer_wage_by_state: 'N/A',
            black: 'N/A',
            city: 'N/A',
            female: 'N/A',
            id: this.props.id,
            male: 'N/A',
            media: 'N/A',
            name: 'N/A',
            native_hawaiian: 'N/A',
            officer_count_per_1000_civs: 'N/A',
            officer_pop: 'N/A',
            ori: 'N/A',
            population: 'N/A',
            state: 'N/A',
            total: 'N/A',
            unknown: 'N/A',
            white: 'N/A',
            male_percent: 'N/A',
            female_percent: 'N/A',
            demo: { id: 'N/A' },
            departments: [],
            services: []
        }
    }

    // Life cycle method used to update state when new props are received
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.id !== prevState.id) {
            return { id: nextProps.id }
        }
        else {
            return null
        }
    }

    // Check whether page should reload or not, preventing infinite loop on 
    // state updates
    componentDidUpdate(nextProps) {
        if (nextProps.id !== this.state.id) {
            this.setState(
                { id: nextProps.id },
                function () { this.fetchData() });
        }
    }


    // Fetch the data for this police dept, then all the depts, social 
    // services, and demographics in the same state
    fetchData() {
        fetch('https://api.copwatchusa.com/data/departments/' + this.props.id)
            .then(response => response.json())
            .then((response) => {
                fetch('https://api.copwatchusa.com/data/departments?state='
                    + response.state)
                    .then(departments => departments.json())
                    .then(departments => {
                        fetch('https://api.copwatchusa.com/data/socialservices?state='
                            + response.state)
                            .then(services => services.json())
                            .then(services => {
                                fetch('https://api.copwatchusa.com/data/demographics?state='
                                    + response.state + '&city=' + response.city)
                                    .then(city_res => city_res.json())
                                    .then(city_res => {
                                        response.demo = city_res;
                                        for (let field in response) {
                                            if (response[field] === 0) {
                                                response[field] = 'N/A'
                                            }
                                        }

                                        // Calculate percents of gender
                                        let male = response.male
                                        let female = response.female
                                        let officer_pop = response.officer_pop
                                        if (female !== 'N/A' && male !== 'N/A' && officer_pop !== 'N/A') {
                                            var male_percent = Math.round(male / officer_pop * 100)
                                            var female_percent = Math.round(female / officer_pop * 100)
                                            response.male_percent = male_percent
                                            response.female_percent = female_percent
                                        }

                                        response.departments = departments;
                                        response.services = services;
                                        this.setState(response);
                                    });
                            });
                    });
            });

        window.scrollTo(0, 0) // Scroll to the top of the page
    }

    componentDidMount() {
        this.fetchData()
        window.scrollTo(0, 0) // Scroll to the top of the page
    }

    // Render the police instance page 
    render() {
        return (

            <div>
                <Jumbotron>
                    <h1>{this.state.name}</h1>
                    <Link to={'/Demographic/' + this.state.demo.id}>
                        <h3>{this.state.city}, {this.state.state}</h3>
                    </Link>
                    <h3>Serving a population of: {NumberFormatter(this.state.population)}</h3>
                </Jumbotron>

                <div className="centered-horizontal">
                    <Row>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>Number of Officers:</Card.Title>
                                <Card.Text>
                                    {NumberFormatter(this.state.officer_pop)}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>Offenses per year:</Card.Title>
                                <Card.Text>for most recent year</Card.Text>
                                <Card.Text>
                                    {NumberFormatter(this.state.total)}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>Average officer wage in
                                 {" "}{this.state.state}:</Card.Title>
                                <Card.Text>
                                    {"$ " + NumberFormatter(this.state.avg_officer_wage_by_state)}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Row>
                </div>

                <div className="centered-horizontal">
                    <Row>
                        <div className="padding-right">
                            <Card style={{ width: '80rem' }}>
                                <Card.Img
                                    variant="top"
                                    src={this.state.media} />
                            </Card>
                        </div>
                    </Row>
                </div>

                <div className="centered-horizontal">
                    <Row>
                        <Col>
                            <Card style={{ width: '20rem' }}>
                                <Card.Body>
                                    <Card.Title> Police Departments in
                                     {" "}{this.state.state} </Card.Title>
                                    <ListDepartments list=
                                        {this.state.departments} />
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{ width: '20rem' }}>
                                <Card.Body>
                                    <Card.Title> Social Services in
                                     {" "}{this.state.state} </Card.Title>
                                    <ListSocials list=
                                        {this.state.services}></ListSocials>
                                </Card.Body>
                            </Card>
                        </Col>
                        <div className="centered">
                            <Col>
                                <Card style={{ width: '18rem' }}>
                                    <Card.Body>
                                        <Card.Title>
                                            There are
                                            {" "}{this.
                                                state.officer_count_per_1000_civs}
                                            {" "}officers for every 1,000 people in
                                            {" "}{this.state.city
                                            }</Card.Title>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: '18rem' }}>
                                    <Card.Body>
                                        <Card.Title>
                                            {this.state.male_percent}%
                                            of officers are male and
                                             {" "}{this.state.female_percent}% of
                                            officers are female.
                                        </Card.Title>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </div>
                    </Row>
                </div>
            </div>
        )
    }
}

// List all related police departments
function ListDepartments(props) {
    const list = props.list.map((pol) =>
        <div key={pol.id}>
            <Link to={"/Police/" + pol.id}>{pol.name}</Link>
        </div>
    );
    return (
        <div>{list}</div>
    );
}

// List all related social services
function ListSocials(props) {
    const list = props.list.map((soc) =>
        <div key={soc.id}>
            <Link to={"/SocialService/" + soc.id}>{soc.type_ss}</Link>
        </div>
    );
    return (
        <div>{list}</div>
    );
}

export default PoliceInstance;