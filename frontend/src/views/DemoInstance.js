import React, { Component } from 'react'
import { Jumbotron } from 'react-bootstrap'
import { Card } from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import { Link } from "react-router-dom"
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend }
    from 'recharts'
import NumberFormatter from '../components/NumberFormatter'
import './Instances.css'




class DemoInstance extends Component {

    constructor(props) {
        super(props)
        this.state = {
            american_indian: 'N/A',
            asian: 'N/A',
            associate_degree: 'N/A',
            average_household_size: 'N/A',
            bachelor_degree: 'N/A',
            black: 'N/A',
            city: 'N/A',
            eighteen_to_twentyfour: 'N/A',
            female: 'N/A',
            fiftyfive_to_sixtyfour: 'N/A',
            five_to_seventeen: 'N/A',
            fourtyfive_to_fiftyfour: 'N/A',
            graduate_professional_degree: 'N/A',
            high_school_graduate: 'N/A',
            id: this.props.id,
            less_than_high_school_diploma: 'N/A',
            male: 'N/A',
            mean_cash_public_assist: 'N/A',
            media: 'N/A',
            median_age: 'N/A',
            median_hh_income: 'N/A',
            native_hawaiian: 'N/A',
            not_health_insured: 'N/A',
            other: 'N/A',
            percent_snap_users: 'N/A',
            place_id: 'N/A',
            population: 'N/A',
            poverty_rate: 'N/A',
            seventyfiveplus: 'N/A',
            sixtyfive_to_seventyfour: 'N/A',
            state: 'N/A',
            state_id: 'N/A',
            thirtyfive_to_fourtyfour: 'N/A',
            twentyfive_to_thirtyfour: 'N/A',
            under_five: 'N/A',
            unemployment_rate: 'N/A',
            white: 'N/A',
            ageData: [],
            departments: [],
            services: []
        }
    }

    componentDidMount() {
        //Use props to generate the right fetch request
        //fetch('https://api.copwatchusa.com/demographics?name=' + 
        // this.props.city)
        //then manipulate the data until we can do:
        //this.setState(*new data from json, written like demoExample*)
        fetch('https://api.copwatchusa.com/data/demographics/' + this.state.id)
            .then(response => response.json())
            .then((response) => {
                fetch('https://api.copwatchusa.com/data/socialservices?state='
                    + response.state)
                    .then(services => services.json())
                    .then(services => {
                        fetch('https://api.copwatchusa.com/data/departments?state='
                            + response.state)
                            .then(departments => departments.json())
                            .then(departments => {
                                let ages = []
                                for (let field in response) {
                                    // Get the population break down by age and save it 
                                    // for the bar chart
                                    if (field === 'five_to_seventeen') {
                                        let pops = { name: '5-17', population: 0 }
                                        pops.population = response.five_to_seventeen
                                        ages.push(pops)
                                    }
                                    else if (field === 'eighteen_to_twentyfour') {
                                        let pops = { name: '18-24', population: 0 }
                                        pops.population = response.five_to_seventeen
                                        ages.push(pops)
                                    }
                                    else if (field === 'twentyfive_to_thirtyfour') {
                                        let pops = { name: '25-34', population: 0 }
                                        pops.population = response.twentyfive_to_thirtyfour
                                        ages.push(pops)
                                    }
                                    else if (field === 'thirtyfive_to_fourtyfour') {
                                        let pops = { name: '35-44', population: 0 }
                                        pops.population = response.thirtyfive_to_fourtyfour
                                        ages.push(pops)
                                    }
                                    else if (field === 'fourtyfive_to_fiftyfour') {
                                        let pops = { name: '44-54', population: 0 }
                                        pops.population = response.fourtyfive_to_fiftyfour
                                        ages.push(pops)
                                    }
                                    else if (field === 'fiftyfive_to_sixtyfour') {
                                        let pops = { name: '55-64', population: 0 }
                                        pops.population = response.fiftyfive_to_sixtyfour
                                        ages.push(pops)
                                    }
                                    else if (field === 'sixtyfive_to_seventyfour') {
                                        let pops = { name: '65-74', population: 0 }
                                        pops.population = response.sixtyfive_to_seventyfour
                                        ages.push(pops)
                                    }
                                    else if (field === 'seventyfiveplus') {
                                        let pops = { name: '75+', population: 0 }
                                        pops.population = response.seventyfiveplus
                                        ages.push(pops)
                                    }
                                    else if (response[field] === 0 || response[field] === null) {
                                        response[field] = 'N/A'
                                    }
                                }
                                response.ageData = ages
                                response.services = services
                                response.departments = departments
                                this.setState(response);
                            },
                                (error) => {
                                    this.setState({
                                        error
                                    });
                                });
                    });
            });

        window.scrollTo(0, 0) // Scroll to the top of the page 
    }

    render() {

        return (

            <div>
                {/* Name, state, population */}
                <Jumbotron>
                    <h1>{this.state.city}, {this.state.state}</h1>
                    <h3>Population Estimate: {NumberFormatter(this.state.population)}</h3>
                </Jumbotron>

                {/* Age, income, and Incarceration */}
                <div className="centered-horizontal">
                    <Row>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>Median Age:</Card.Title>
                                <Card.Text>
                                    {this.state.median_age}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>
                                    Median Household Income:
                                </Card.Title>
                                <Card.Text>
                                    {NumberFormatter(this.state.median_hh_income)}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                        <Card style={{ width: '18rem' }}>
                            <Card.Body>
                                <Card.Title>Unemployment Rate:</Card.Title>
                                <Card.Text>
                                    {this.state.unemployment_rate}%
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Row>
                </div>

                {/* Image of Region */}
                <div className="centered-horizontal">
                    <Row>
                        <div className="padding-right">
                            <Card style={{ width: '60rem' }}>
                                <Card.Img variant="top"
                                    src={this.state.media} />
                            </Card>
                        </div>
                    </Row>
                </div>

                {/* Additional Stats */}
                <div className="centered-horizontal">
                    <Row>
                        <div className="centered">
                            <Col>
                                <Card style={{ width: '18rem' }}>
                                    <Card.Body>
                                        <Card.Title>
                                            {this.state.percent_snap_users}%
                                            of the population uses SNAP
                                        </Card.Title>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: '18rem' }}>
                                    <Card.Body>
                                        <Card.Title>
                                            {this.state.poverty_rate}%
                                            of the population lives in poverty
                                        </Card.Title>
                                    </Card.Body>
                                </Card>
                                <Card style={{ width: '18rem' }}>
                                    <Card.Body>
                                        <Card.Title>
                                            {this.state.not_health_insured}%
                                            of the population does not have
                                            health insurance
                                        </Card.Title>
                                    </Card.Body>
                                </Card>
                            </Col>
                        </div>

                        {/* Make the population break down bar chart */}
                        <Card>
                            <Card.Body>
                                <Card.Title>Population Age</Card.Title>
                                <BarChart
                                    width={500}
                                    height={300}
                                    data={this.state.ageData}
                                    margin={{
                                        top: 5, right: 30, left: 20, bottom: 5,
                                    }}
                                >
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="population" fill="#8884d8" />
                                </BarChart>
                            </Card.Body>
                        </Card>
                    </Row>
                </div>

                <div className="centered-horizontal">
                    <Row>
                        <Col>
                            <Card style={{ width: '20rem' }}>
                                <Card.Body>
                                    <Card.Title> Social Services in
                                        {" "}{this.state.state}
                                    </Card.Title>
                                    <ListSocials list=
                                        {this.state.services}>
                                    </ListSocials>
                                </Card.Body>
                            </Card>
                        </Col>
                        <Col>
                            <Card style={{ width: '25rem' }}>
                                <Card.Body>
                                    <Card.Title>
                                        Police Departments in
                                        {" "}{this.state.state}
                                    </Card.Title>
                                    <ListDepartments
                                        list={this.state.departments} />
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </div>
        )
    }
}


//List all related Social Services
function ListSocials(props) {
    const list = props.list.map((soc) =>
        <div key={soc.id}>
            <Link to={"/SocialService/" + soc.id}>{soc.type_ss}</Link>
        </div>
    );
    return (
        <div>{list}</div>
    );
}

//List all related police departments
function ListDepartments(props) {
    const list = props.list.map((pol) =>
        <div key={pol.id}>
            <Link to={"/Police/" + pol.id}>{pol.name}</Link>
        </div>
    );
    return (
        <div>{list}</div>
    );
}

export default DemoInstance;