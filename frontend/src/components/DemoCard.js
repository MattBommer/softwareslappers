import React from 'react'
import { Card } from 'react-bootstrap'
import { useHistory } from 'react-router-dom';
import NumberFormatter from './NumberFormatter'

// The code for this pagination comes from this website: 
// https://www.digitalocean.com/


const DemoCard = props => {

    // Store the values from the prop that we will use
    const {
        city = null,
        state = null,
        population = 'N/A',
        poverty_rate = 'N/A',
        unemployment_rate = 'N/A',
        id = null,
        media = null
    } = props.city || {};

    const history = useHistory()

    return (
        <div className='padding-all'>
            <a style={{ cursor: 'pointer' }} href={id} onClick={() => {
                history.push("Demographic/" + id)
            }}>
                <Card style={{ width: '14rem' }}>
                    <Card.Img 
                        variant="top" className="demo-card-photo" src={media}/>
                    <Card.Body>
                        <Card.Title>{city}, {state}</Card.Title>
                        <Card.Text>
                            Population: {NumberFormatter(population)}
                            </Card.Text>
                        <Card.Text>
                            Poverty Rate: {poverty_rate !== null 
                            ? poverty_rate + '%' : 'N/A'}
                        </Card.Text>
                        <Card.Text>
                            Unemployment Rate: {unemployment_rate}%
                        </Card.Text>
                    </Card.Body>
                </Card>
            </a>
        </div>


    )
}



export default DemoCard;