import React from 'react'
import { Card } from 'react-bootstrap'
import { PieChart, Pie, Legend, Tooltip, ResponsiveContainer, Cell} from 'recharts'

const COLORS = ['#003f5c', '#7a5195', '#ef5675', '#ffa600'];

const MedtroPieChart = props => {
    return (
        <div className='padding-all'>
            <Card>
                <Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <ResponsiveContainer className="center-in-container" width="95%" height={600}>
                        <PieChart
                            width={500}
                            height={500}
                        >
                            <Tooltip />
                            <Legend />
                            <Pie name="gender" data={props.data} dataKey="value" outerRadius={250} fill="#8884d8" >
                                {props.data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)}
                            </Pie>
                        </PieChart>
                    </ResponsiveContainer>
                </Card.Body>
            </Card>
        </div>
    )
}



export default MedtroPieChart;