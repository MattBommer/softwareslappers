import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import '../stylesheets/copwatch.css'


// Returns the footer component
class Footer extends Component {
	render () {
		return (
		    <footer className="page-footer bg-dark pt-4">
		       <div className="container-fluid text-center text-md-left">
		          <div className="row">
		             <div className="col-md-7 mt-md-0 mt-3">
		                <h3 className="text-light">CopWatchUSA</h3>
		                <p className="text-light">
							Tracking police efficacy throughout the US in 
							relation to religious data, socioeconomic data, 
							education, police funding, types of arrests, 
							police violence, and community incarceration rates.
						</p>
		             </div>
		            <div className="col-md-2 mb-md-0 mb-3">
		                <h5 className="text-uppercase text-light">Models</h5>
		                <ul className="list-unstyled text-light">
		                   <li><div className="text-light">
							    <Link to="/PoliceDepartments"> 
							   		Police Departments 
								</Link></div></li>
		                   <li><div className="text-light">
								<Link to="/Demographics"> 
									Demographics 
								</Link></div></li>
		                   <li><div className="text-light">
							    <Link to="/SocialServices"> 
									Social Services 
							   	</Link></div></li>
		                </ul>
		            </div>
		             <div className="col-md-2 mb-md-0 mb-3">
		                <h5 className="text-uppercase text-light">
							Other Links
						</h5>
		                <ul className="list-unstyled text-light">
		                   <li><div className="text-light">
							   <Link to="/"> 
							   	Home
							   </Link></div></li>
		                   <li><div className="text-light">
							   <Link to="/About"> 
								About 
								</Link></div></li>
		                </ul>
		             </div>
		          </div>
		       </div>
		       <div className="footer-copyright text-center text-light py-3">
				   © 2020 Copyright:<div className="text-light">
					   <Link to="/"> CopWatchUSA.com </Link>
					</div></div>
		    </footer>
		);
	}
}

export default Footer;