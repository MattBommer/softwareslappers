import React from 'react'
import { Card } from 'react-bootstrap'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, 
    ResponsiveContainer, Cell}
    from 'recharts'

const COLORS = ['#003f5c', '#7a5195', '#ef5675', '#ffa600'];

const MedtroBarChart = props => {
    return (
        <div className='padding-all'>
            <Card>
                <Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <ResponsiveContainer className="center-in-container" width="95%" height={600}>
                        <BarChart
                            width={400}
                            height={600}
                            data={props.data}
                            margin={{
                                top: 50, right: 20, left: 20, bottom: 50,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="city" angle={-45} textAnchor="end" />
                            <YAxis />
                            <Tooltip />
                            <Bar dataKey="doctors" >
                                {props.data.map((entry, index) => <Cell fill={COLORS[index % COLORS.length]}/>)}
                            </Bar>
                        </BarChart>
                    </ResponsiveContainer>
                </Card.Body>
            </Card>
        </div>
    )
}



export default MedtroBarChart;