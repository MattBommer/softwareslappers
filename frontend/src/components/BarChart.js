import React from 'react'
import { Card } from 'react-bootstrap'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, 
    ResponsiveContainer }
    from 'recharts'

const COLORS = ['#003f5c', '#7a5195'];

const SSBarChart = props => {
    return (
        <div className='padding-all'>
            <Card>
                <Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <ResponsiveContainer width="95%" height={600}>
                        <div className = "chart-overflow">
                            <BarChart
                                width={8000}
                                height={600}
                                data={props.data}
                                margin={{
                                    top: 5, right: 30, left: 20, bottom: 100,
                                }}
                            >
                                <CartesianGrid strokeDasharray="3 3" />
                                <XAxis dataKey="state" angle={-45} textAnchor="end" />
                                <YAxis />
                                <Tooltip />
                                <Legend layout="vertical" verticalAlign="middle" align="left" />
                                <Bar dataKey="dependents" fill={COLORS[0]} />
                                <Bar dataKey="households" fill={COLORS[1]} />
                            </BarChart>
                        </div>
                    </ResponsiveContainer>
                </Card.Body>
            </Card>
        </div>
    )
}



export default SSBarChart;