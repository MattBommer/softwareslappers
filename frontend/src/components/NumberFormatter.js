

// Function that returns a formatted version of a large number
function NumberFormatter (num) {
    
    // Put a comma at any point that has exactly 3 digits in a row after it.
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

export default NumberFormatter