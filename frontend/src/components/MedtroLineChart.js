import React from 'react'
import { Card } from 'react-bootstrap'
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer} from 'recharts';

const MedtroLineChart = props => {
	return (
		<div className='padding-all'>
			<Card>
				<Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <ResponsiveContainer className="center-in-container" width="95%" height={600}>
                    	<LineChart name="Services" data={props.data} width={500} height={350}>
                    	<CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="city" angle={0} textAnchor="end" />
                        <YAxis/>
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="services" stroke="#003f5c" activeDot={{r:8}}/>
                    	</LineChart>
                    </ResponsiveContainer>
                </Card.Body>
			</Card>
		</div>
	)
}

export default MedtroLineChart