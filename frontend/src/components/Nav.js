import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import '../stylesheets/copwatch.css'

class MyNav extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Active: "1"
        }
    }

    // Render the nav bar component
    render() {
        return (
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand as={Link} to="/">CopWatchUSA</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="Dynamic NavBar">
                    <Nav activeKey={this.props.active}>
                        <Nav.Link eventKey="1" as={Link} to="/">Home</Nav.Link>
                        <Nav.Link eventKey="2" as={Link}
                            to="/PoliceDepartments">Police Departments
                        </Nav.Link>
                        <Nav.Link eventKey="3" as={Link}
                            to="/Demographics">Demographics
                        </Nav.Link>
                        <Nav.Link eventKey="4" as={Link}
                            to="/SocialServices">Social Services
                        </Nav.Link>
                        <Nav.Link eventKey="5" as={Link}
                            to="/About">About
                        </Nav.Link>
                        <Nav.Link eventKey="6" as={Link}
                            to="/Visualizations">Visualizations
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default MyNav;