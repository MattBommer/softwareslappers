import React from 'react'
import { Card } from 'react-bootstrap'
import { PieChart, Pie, Legend, Tooltip,  ResponsiveContainer, Cell} from 'recharts'

const COLOR1 = ['#003f5c', '#7a5195'];
const COLOR2 = ['#ef5675', '#ffa600'];

const DepPieChart = props => {
    return (
        <div className='padding-all'>
            <Card>
                <Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <ResponsiveContainer className="center-in-container" width="95%" height={600}>
                        <PieChart 
                            width={500}
                            height={500}
                        >
                            <Tooltip />
                            <Legend />
                            <Pie name="national" data={props.data1} dataKey="value" outerRadius={80}>
                                {props.data1.map((entry, index) => <Cell fill={COLOR1[index % COLOR1.length]}/>)}
                            </Pie>
                            <Pie name="police" data={props.data2} dataKey="value" innerRadius={110} outerRadius={140} label>
                                {props.data2.map((entry, index) => <Cell fill={COLOR2[index % COLOR2.length]}/>)}
                            </Pie>
                        </PieChart>
                    </ResponsiveContainer>
                </Card.Body>
            </Card>
        </div>
    )
}



export default DepPieChart;