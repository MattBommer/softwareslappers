import React from 'react'
import { Container } from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import { Card } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import Highlighter from './Highlighter'
import '../views/Instances.css'

const ServiceSearchEntry = props => {
	
	// Store the values from the prop that we will use
	const {
		type_ss = null,
		state = null,
		id = null,
		media = null,
		avg_funds_per_year = null,
		dependent_households = null
	} = props.res || {};

	const query = props.query
	const history = useHistory()
	const desc = type_ss + " supports " + dependent_households + 
		" households a year with $" + avg_funds_per_year

	return (
		<a style={{ cursor: 'pointer' }} href={id} onClick={() => {
			history.push("/SocialService/" + id)
		}}>
			<Container className="centered-horizontal">
				<Card style={{ width: "100vw", height: "200px" }}>
					<Row>
						<Col md="auto">
							<Card.Img variant="left" 
								src={media} 
								className="search-photo" />
						</Col>
						<Col md="auto">
							<Card.Body>
								<Card.Title>
									{Highlighter
										(type_ss + " in " + state, query)}
								</Card.Title>
								<Card.Text>
									{Highlighter(desc, query)}
								</Card.Text>
							</Card.Body>
						</Col>
					</Row>
				</Card>
			</Container>
		</a>
	);

}

export default ServiceSearchEntry