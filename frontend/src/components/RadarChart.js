import React from 'react'
import { Card } from 'react-bootstrap'
import { RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Radar, 
    Legend, ResponsiveContainer } from 'recharts'

const COLORS = ['#003f5c', '#7a5195'];

const RaceRadarChart = props => {
    return (
        <div className='padding-all'>
            <Card>
                <Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <ResponsiveContainer className="center-in-container" width="95%" height={600}>
                        <RadarChart outerRadius={180} width={600} height={600} data={props.data}>
                            <PolarGrid />
                            <PolarAngleAxis dataKey="race" />
                            <PolarRadiusAxis angle={30} domain={[0, 65]} />
                            <Radar name="Racial Composition of all Cities" dataKey="demographics" stroke={COLORS[0]} fill={COLORS[0]} fillOpacity={0.6} />
                            <Radar name="Racial Composition of all Offenders" dataKey="offender" stroke={COLORS[1]} fill={COLORS[1]}  fillOpacity={0.6} />
                            <Legend />
                        </RadarChart>
                    </ResponsiveContainer>
                </Card.Body>
            </Card>
        </div>
    )
}



export default RaceRadarChart;