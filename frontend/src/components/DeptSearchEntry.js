import React from 'react'
import { Container } from 'react-bootstrap'
import { Row } from 'react-bootstrap'
import { Card } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import { useHistory } from 'react-router-dom';
import Highlighter from './Highlighter'
import '../views/Instances.css'


const DeptSearchEntry = props => {

    // Store the values from the prop that we will use
	const {
		name = null,
		id = null,
		media = null,
		officer_pop = null
	} = props.res || {};

	const query = props.query
	const history = useHistory()
	const desc = name + " employs " + officer_pop + " officers."


	return (
		<a style={{ cursor: 'pointer' }} href={id} onClick={() => {
			history.push("/Police/" + id)
		}}>
			<Container className="centered-horizontal">
				<Card style={{ width: "100vw", height: "200px" }}>
					<Row>
						<Col md="auto">
							<Card.Img 
							variant="left" 
							src={media} 
							className="search-photo" />
						</Col>
						<Col md="auto">
							<Card.Body>
								<Card.Title>
									{Highlighter(name, query)}
								</Card.Title>
								<Card.Text>
									{Highlighter(desc, query)}
								</Card.Text>
							</Card.Body>
						</Col>
					</Row>
				</Card>
			</Container>
		</a>
	);

}

export default DeptSearchEntry