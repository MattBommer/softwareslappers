import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch, useParams } 
  from "react-router-dom";
import Footer from './components/Footer'
import MyNav from './components/Nav'
import Homepage from './views/Homepage';
import About from './views/About';
import Demo from './views/Demographics';
import Social from './views/SocialServices';
import SearchPage from './views/Search';
import Police from './views/Police';
import InstView from './views/InstanceView';
import PoliceInstance from './views/PoliceInstance';
import SocialInstance from './views/SocialInstance';
import DemoInstance from './views/DemoInstance';
import Visuals from './views/Visualizations'
import './stylesheets/copwatch.css';
import './App.css'


class App extends Component {
  //App.js routes between components based on their exact links. For instances
  //and the search page, we use the useParams hook to pass in the proper props 
  render() {
    return (
      <Router>
        <div className="App">
          <Switch>
            <Route exact path={'/'} component={Homepage}>
            </Route>
            <Route exact path={'/Demographics'} component={Demo} />
            <Route exact path={'/SocialServices'} component={Social} />
            <Route exact path={'/PoliceDepartments'} component={Police} />
            <Route exact path={'/About'} component={About} />
            <Route exact path={'/InstanceView'} component={InstView} />
            <Route exact path={'/Police/:id'} component={Pol} />
            <Route exact path={'/Demographic/:id'} component={Dem} />
            <Route exact path={'/SocialService/:id'} component={Soc} />
            <Route exact path={'/Search/:id'} component={Sr} />
            <Route exact path={'/Visualizations/'} component={Visuals} />
          </Switch>
        </div>
        <Footer />
      </Router>
    );
  }
}

//Helper to load the search page properly
function Sr() {
  let { id } = useParams();
  return (
    <div>
      <MyNav active="1" />
      <SearchPage query={id} />
    </div>
  )
}
//Helper to load a social service instance
function Soc() {
  let { id } = useParams();
  return (
    <div>
      <MyNav active="4" />
      <SocialInstance id={id} />
    </div>
  );
}
//Helper to load a police instance
function Pol() {
  let { id } = useParams();
  return (
    <div>
      <MyNav active="2" />
      <PoliceInstance id={id} />
    </div>
  );
}
//Helper to load a demographics instance
function Dem() {
  let { id } = useParams();
  return (
    <div>
      <MyNav active="3" />
      <DemoInstance id={id} />
    </div>
  );
}


export default App;
