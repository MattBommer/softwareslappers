import React from 'react'
import { shallow } from 'enzyme'

import App from '../src/App'
import Homepage from '../src/views/Homepage'
import DemoInstance from '../src/views/DemoInstance'
import PoliceInstance from '../src/views/PoliceInstance'
import SocialInstance from '../src/views/SocialInstance'
import Demographics from '../src/views/Demographics'
import Police from '../src/views/Police'
import SocialServices from '../src/views/SocialServices'
import DemoCard from '../src/components/DemoCard'
import Footer from '../src/components/Footer'
import Nav from '../src/components/Nav'
import Pagination from '../src/components/Pagination'


// Tells jest to use fakeData instead of making GET request
const fakeData = {}
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(fakeData),
  })
);

describe('Renders without crashing', () => {
    test('App', () => {
        // Render the app component
        const test = shallow(<App />); 
        expect(test).toMatchSnapshot();
    });
    test('Homepage', () => {
        // Render the homepage component
        const test = shallow(<Homepage />); 
        expect(test).toMatchSnapshot();
    });
    test('Demographics Instance', () => {
        // Render the Demoinstance component
        const test = shallow(<DemoInstance />); 
        expect(test).toMatchSnapshot();
    });
    test('Police Instance', () => {
        // Render the PoliceInstance component
        const test = shallow(<PoliceInstance />); 
        expect(test).toMatchSnapshot();
    });
    test('Social Instance', () => {
        // Render the SocialInstance component
        const test = shallow(<SocialInstance />); 
        expect(test).toMatchSnapshot();
    });
    test('Demographics Model', () => {
        // Render the Demographics Model component
        const test = shallow(<Demographics />); 
        expect(test).toMatchSnapshot();
    });
    test('Police Model', () => {
        // Render the Police Departments Model component
        const test = shallow(<Police />); 
        expect(test).toMatchSnapshot();
    });
    test('SocialServices Model', () => {
        // Render the Social Services Model component
        const test = shallow(<SocialServices />); 
        expect(test).toMatchSnapshot();
    });
    test('DemoCard', () => {
        // Render the DemoCard component
        const test = shallow(<DemoCard />); 
        expect(test).toMatchSnapshot();
    });
    test('Footer', () => {
        // Render the Footer component
        const test = shallow(<Footer />); 
        expect(test).toMatchSnapshot();
    });
    test('Nav', () => {
        // Render the Nav component
        const test = shallow(<Nav />); 
        expect(test).toMatchSnapshot();
    });
    test('Pagination', () => {
        // Render the Pagination component
        const test = shallow(<Pagination />); 
        expect(test).toMatchSnapshot();
    });
  });