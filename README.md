# Members (Name, EID, and GitlabID): 
-  Matt Bommer, mkb2683, MattBommer  
-  Kim Ha, kth675, kim-ha
-  Michael Kajihara, mrk2646, mkajihara
-  Jarod Norwood, jcn2289, j.norwood13

# Last Git SHA:
a037c0e5df0841032ed9e6b87e7d07a80a338f38

# Project Leader: 
Matt Bommer

# Gitlab pipeline(s) (link): 
[Test & Deployment Pipeline](https://gitlab.com/MattBommer/softwareslappers/-/pipelines)

The above pipeline performs all of the testing for backend unittests, frontend unittests, and postman api tests.
In addition the pipeline includes and external buddy pipeline that deploys our react app to our AWS S3 bucket and refreshs the cloudfront cache to reflect the changes.

# Website (link): 
[CopWatchUSA](https://copwatchusa.com)

# Estimated completion time for each member (hours: int): 
- Matt: 10
- Kim: 8
- Michael: 11
- Jarod: 5

# Actual completion time for each member (hours: int)
Actual completion time was right around 7 hours

# Additional Comments:
### No additional comments.
