from app import db

# This is where the models for each table within the database are
# defined, so that the db can be interacted with abstractly.

# This model describes the unittest table in the database that tracks the
# unit tests for each project member.
class UnitTests(db.Model):
    __tablename__ = "unittest"

    id = db.Column(db.Integer, primary_key=True)
    total = db.Column(db.Integer)
    matt_unit = db.Column(db.Integer)
    michael_unit = db.Column(db.Integer)
    kim_unit = db.Column(db.Integer)
    jarod_unit = db.Column(db.Integer)

    def __init__(self, total, matt_unit, michael_unit, kim_unit, jarod_unit):
        self.total = total
        self.matt_unit = matt_unit
        self.michael_unit = michael_unit
        self.kim_unit = kim_unit
        self.jarod_unit = jarod_unit

    def __repr__(self):
        return "<Total %r>" % self.total


# This model describes the department table in the database that tracks
# information related to a specific police department such as location,
# racial arrest data, and wage.
class Departments(db.Model):
    __tablename__ = "departments"

    id = db.Column(db.Integer, primary_key=True)
    ori = db.Column(db.String(50))
    name = db.Column(db.String(50))
    city = db.Column(db.String(50))
    state = db.Column(db.String(50))
    population = db.Column(db.Integer)
    officer_pop = db.Column(db.Integer)
    male = db.Column(db.Integer)
    female = db.Column(db.Integer)
    officer_count_per_1000_civs = db.Column(db.Float)
    asian = db.Column(db.Integer)
    native_hawaiian = db.Column(db.Integer)
    black = db.Column(db.Integer)
    american_indian = db.Column(db.Integer)
    unknown = db.Column(db.Integer)
    white = db.Column(db.Integer)
    total = db.Column(db.Integer)
    media = db.Column(db.String(80))
    avg_officer_wage_by_state = db.Column(db.Float)

    def __init__(
        self,
        ori,
        name,
        city,
        state,
        population,
        officer_pop,
        male,
        female,
        officer_count_per_1000_civs,
        asian,
        native_hawaiian,
        black,
        american_indian,
        unknown,
        white,
        total,
        media,
        avg_offiver_wage_by_state,
    ):
        self.ori = ori
        self.name = name
        self.city = city
        self.state = state
        self.population = population
        self.officer_pop = officer_pop
        self.male = male
        self.female = female
        self.officer_count_per_1000_civs = officer_count_per_1000_civs
        self.asian = asian
        self.native_hawaiian = native_hawaiian
        self.black = black
        self.american_indian = american_indian
        self.unknown = unknown
        self.white = white
        self.total = total
        self.media = media
        self.avg_offiver_wage_by_state = avg_offiver_wage_by_state

    def __repr__(self):
        return "<Name %r>" % self.name


# This model describes the demographics table in the database which stores
# data regarding the demographics of a specific city primarily obtained from
# the US Census.
class Demographics(db.Model):
    __tablename__ = "demographics"

    id = db.Column(db.Integer, primary_key=True)
    american_indian = db.Column(db.Integer)
    asian = db.Column(db.Integer)
    associate_degree = db.Column(db.Integer)
    average_household_size = db.Column(db.Float)
    bachelor_degree = db.Column(db.Integer)
    black = db.Column(db.Integer)
    city = db.Column(db.String(50))
    eighteen_to_twentyfour = db.Column(db.Integer)
    female = db.Column(db.Integer)
    fiftyfive_to_sixtyfour = db.Column(db.Integer)
    five_to_seventeen = db.Column(db.Integer)
    fourtyfive_to_fiftyfour = db.Column(db.Integer)
    graduate_professional_degree = db.Column(db.Integer)
    high_school_graduate = db.Column(db.Integer)
    less_than_high_school_diploma = db.Column(db.Integer)
    male = db.Column(db.Integer)
    mean_cash_public_assist = db.Column(db.Integer)
    median_age = db.Column(db.Float)
    native_hawaiian = db.Column(db.Integer)
    median_hh_income = db.Column(db.Integer)
    not_health_insured = db.Column(db.Float)
    other = db.Column(db.Integer)
    percent_snap_users = db.Column(db.Float)
    place_id = db.Column(db.Integer)
    population = db.Column(db.Integer)
    poverty_rate = db.Column(db.Float)
    seventyfiveplus = db.Column(db.Integer)
    sixtyfive_to_seventyfour = db.Column(db.Integer)
    state = db.Column(db.String(50))
    state_id = db.Column(db.Integer)
    thirtyfive_to_fourtyfour = db.Column(db.Integer)
    twentyfive_to_thirtyfour = db.Column(db.Integer)
    under_five = db.Column(db.Integer)
    unemployment_rate = db.Column(db.Float)
    white = db.Column(db.Integer)
    media = db.Column(db.String(80))

    def __init__(
        self,
        american_indian,
        asian,
        associate_degree,
        average_household_size,
        bachelor_degree,
        black,
        city,
        eighteen_to_twentyfour,
        female,
        fiftyfive_to_sixtyfour,
        five_to_seventeen,
        fourtyfive_to_fiftyfour,
        graduate_professional_degree,
        high_school_graduate,
        id,
        less_than_high_school_diploma,
        male,
        mean_cash_public_assist,
        median_age,
        native_hawaiian,
        median_hh_income,
        not_health_insured,
        other,
        percent_snap_users,
        place_id,
        population,
        poverty_rate,
        seventyfiveplus,
        sixtyfive_to_seventyfour,
        state,
        state_id,
        thirtyfive_to_fourtyfour,
        twentyfive_to_thirtyfour,
        under_five,
        unemployment_rate,
        white,
        media,
    ):
        self.american_indian = american_indian
        self.asian = asian
        self.associate_degree = associate_degree
        self.average_household_size = average_household_size
        self.bachelor_degree = bachelor_degree
        self.black = black
        self.city = city
        self.eighteen_to_twentyfour = eighteen_to_twentyfour
        self.female = female
        self.fiftyfive_to_sixtyfour = fiftyfive_to_sixtyfour
        self.five_to_seventeen = five_to_seventeen
        self.fourtyfive_to_fiftyfour = fourtyfive_to_fiftyfour
        self.graduate_professional_degree = graduate_professional_degree
        self.high_school_graduate = high_school_graduate
        self.less_than_high_school_diploma = less_than_high_school_diploma
        self.male = male
        self.mean_cash_public_assist = mean_cash_public_assist
        self.median_age = median_age
        self.native_hawaiian = native_hawaiian
        self.median_hh_income = median_hh_income
        self.not_health_insured = not_health_insured
        self.other = other
        self.percent_snap_users = percent_snap_users
        self.place_id = place_id
        self.population = population
        self.poverty_rate = poverty_rate
        self.seventyfiveplus = seventyfiveplus
        self.sixtyfive_to_seventyfour = sixtyfive_to_seventyfour
        self.state = state
        self.state_id = state_id
        self.thirtyfive_to_fourtyfour = thirtyfive_to_fourtyfour
        self.twentyfive_to_thirtyfour = twentyfive_to_thirtyfour
        self.under_five = under_five
        self.unemployment_rate = unemployment_rate
        self.white = white
        self.media = media

    def __repr__(self):
        return "<City %r>" % (self.city)


# This model describes the socialservices table in the database which stores
# use and funding information for a specific social service by state.
class SocialServices(db.Model):
    __tablename__ = "socialservices"

    id = db.Column(db.Integer, primary_key=True)
    type_ss = db.Column(db.String(50))
    state = db.Column(db.String(50))
    avg_funds_per_month = db.Column(db.BigInteger)
    avg_funds_per_year = db.Column(db.BigInteger)
    percent_change_funds_year = db.Column(db.Float)
    dependent_households = db.Column(db.BigInteger)
    avg_monthly_benefits_per_household = db.Column(db.Integer)
    percent_change_dependent_households = db.Column(db.Float)
    dependent_persons = db.Column(db.BigInteger)
    avg_monthly_benefits_per_person = db.Column(db.Integer)
    percent_change_dependent_persons = db.Column(db.Float)
    media = db.Column(db.String(80))

    def __init__(
        self,
        type_ss,
        state,
        avg_funds_per_month,
        avg_funds_per_year,
        percent_change_funds_year,
        dependent_households,
        avg_monthly_benefits_per_household,
        percent_change_dependent_households,
        dependent_persons,
        avg_monthly_benefits_per_person,
        percent_change_dependent_persons,
        media,
    ):
        self.type_ss = type_ss
        self.state = state
        self.avg_funds_per_month = avg_funds_per_month
        self.avg_funds_per_year = avg_funds_per_year
        self.percent_change_funds_year = percent_change_funds_year
        self.dependent_households = dependent_households
        self.avg_monthly_benefits_per_household = (
            avg_monthly_benefits_per_household
        )
        self.percent_change_dependent_households = (
            percent_change_dependent_households
        )
        self.dependent_persons = dependent_persons
        self.avg_monthly_benefits_per_person = avg_monthly_benefits_per_person
        self.percent_change_dependent_persons = percent_change_dependent_persons
        self.media = media

    def __repr__(self):
        return "<State %r>" % (self.state)
