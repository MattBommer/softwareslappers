from marshmallow import Schema, fields

# This file stores the schemas for each model so that they can be easily
# serialized and returned in JSON format by the API

# Schema that describes the columns and associated data types of the
# Departments model
class DepartmentSchema(Schema):
    id = fields.Int()
    ori = fields.Str()
    name = fields.Str()
    city = fields.Str()
    state = fields.Str()
    population = fields.Int()
    officer_pop = fields.Int()
    male = fields.Int()
    female = fields.Int()
    officer_count_per_1000_civs = fields.Float()
    asian = fields.Int()
    native_hawaiian = fields.Int()
    black = fields.Int()
    american_indian = fields.Int()
    unknown = fields.Int()
    white = fields.Int()
    total = fields.Int()
    media = fields.Str()
    avg_officer_wage_by_state = fields.Float()


# Schema that describes the columns and associated data types of the
# Demographics model
class DemographicSchema(Schema):
    # Sets the order of the attributes displayed in the retured JSON to
    # the order in which they are defined in this schema
    class Meta:
        ordered = True

    id = fields.Int()
    american_indian = fields.Int()
    asian = fields.Int()
    associate_degree = fields.Int()
    average_household_size = fields.Float()
    bachelor_degree = fields.Int()
    black = fields.Int()
    city = fields.Str()
    five_to_seventeen = fields.Int()
    eighteen_to_twentyfour = fields.Int()
    twentyfive_to_thirtyfour = fields.Int()
    thirtyfive_to_fourtyfour = fields.Int()
    fourtyfive_to_fiftyfour = fields.Int()
    fiftyfive_to_sixtyfour = fields.Int()
    sixtyfive_to_seventyfour = fields.Int()
    seventyfiveplus = fields.Int()
    female = fields.Int()
    graduate_professional_degree = fields.Int()
    high_school_graduate = fields.Int()
    less_than_high_school_diploma = fields.Int()
    male = fields.Int()
    mean_cash_public_assist = fields.Int()
    median_age = fields.Float()
    native_hawaiian = fields.Int()
    median_hh_income = fields.Int()
    not_health_insured = fields.Float()
    other = fields.Int()
    percent_snap_users = fields.Float()
    place_id = fields.Int()
    population = fields.Int()
    poverty_rate = fields.Float()
    state = fields.Str()
    state_id = fields.Int()
    under_five = fields.Int()
    unemployment_rate = fields.Float()
    white = fields.Int()
    media = fields.Str()


# Schema that describes the columns and associated data types of the
# SocialServices model
class SocialServiceSchema(Schema):
    id = fields.Int()
    type_ss = fields.Str()
    state = fields.Str()
    avg_funds_per_month = fields.Int()
    avg_funds_per_year = fields.Int()
    percent_change_funds_year = fields.Float()
    dependent_households = fields.Int()
    avg_monthly_benefits_per_household = fields.Int()
    percent_change_dependent_households = fields.Float()
    dependent_persons = fields.Int()
    avg_monthly_benefits_per_person = fields.Int()
    percent_change_dependent_persons = fields.Float()
    media = fields.Str()
