from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from config import DevelopmentConfig, ProductionConfig

# This is the base where the Flask app and database exist and
# where the app config is loaded.

app = Flask(__name__)
app.config.from_object(ProductionConfig())
CORS(app)
db = SQLAlchemy(app)
