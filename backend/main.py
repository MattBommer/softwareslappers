from waitress import serve
from app import app, db
from api import api_setup
from models import *

# This ties all the bits and pieces together and
# runs the app on a waitress production server.

api_setup()

if __name__ == "__main__":
    serve(app, host="0.0.0.0", port=5000)
