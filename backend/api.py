from flask_restless import APIManager
from flask_sqlalchemy import SQLAlchemy
from flask_restful import request, Resource, Api
from app import app, db
from models import UnitTests, Departments, Demographics, SocialServices
from schemas import DepartmentSchema, DemographicSchema, SocialServiceSchema
import string

# This creates the endpoints for the api from our SQLAlchemy models defined
# in models.py using Flask resourceful routing for our custom endpoints and
# Flask restless's API Manager for the general API calls.

# The filtering endpoints return all instances of a model if there are
# no arguments provided and then filters those instances by the
# relevant attributes if arguments ARE provided.


# Formats the query arguments to only have the first letter of each word in
# the argument be capitalized, the way attributes appear in the db.
def cap_format(str):
    if str is None:
        return None
    return string.capwords(str.lower())


# Allows filtering of departments using (or not using) state and city
# query parameters:
# EX) https://api.copwatchusa.com/data/departments?state=Texas&city=Austin
class FilterDepartments(Resource):
    def get(self):
        args = request.args
        state = cap_format(args.get("state"))
        city = cap_format(args.get("city"))
        schema = DepartmentSchema(many=True)
        if state is None and city is None:
            depts = Departments.query.all()
        elif state is None:
            depts = Departments.query.filter(Departments.city == city).all()
        elif city is None:
            depts = Departments.query.filter(Departments.state == state).all()
        else:
            depts = Departments.query.filter(
                Departments.state == state, Departments.city == city
            ).first()
            schema = DepartmentSchema()
        result = schema.dump(depts)
        return result


# Allows filtering of demographics using (or not using) state and city
# query parameters:
# EX) https://api.copwatchusa.com/data/demographics?state=Texas&city=Austin
class FilterDemographics(Resource):
    def get(self):
        args = request.args
        state = cap_format(args.get("state"))
        city = cap_format(args.get("city"))
        schema = DemographicSchema(many=True)
        if state is None and city is None:
            dems = Demographics.query.all()
        elif state is None:
            dems = Demographics.query.filter(Demographics.city == city).all()
        elif city is None:
            dems = Demographics.query.filter(Demographics.state == state).all()
        else:
            dems = Demographics.query.filter(
                Demographics.state == state, Demographics.city == city
            ).first()
            schema = DemographicSchema()
        result = schema.dump(dems)
        return result


# Allows filtering of departments using (or not using) state query parameters:
# EX) https://api.copwatchusa.com/data/socialservices?state=Texas
class FilterSocialServices(Resource):
    def get(self):
        args = request.args
        state = cap_format(args.get("state"))
        schema = SocialServiceSchema(many=True)
        if state is None:
            socs = SocialServices.query.all()
        else:
            socs = SocialServices.query.filter(
                SocialServices.state == state
            ).all()
        result = schema.dump(socs)
        return result


def api_setup():
    # CUSTOM ENDPOINTS: Adding Flask resources to API
    api = Api(app)
    api.add_resource(FilterDepartments, "/data/departments")
    api.add_resource(FilterDemographics, "/data/demographics")
    api.add_resource(FilterSocialServices, "/data/socialservices")

    # SIMPLE ENDPOINTS: Adding Flask restless generated endpoints
    manager = APIManager(app, flask_sqlalchemy_db=db)
    # Filtering by id (return 1):
    # https://api.copwatchusa.com/data/<model>/<id>
    dep_blueprint = manager.create_api(
        Departments,
        collection_name="departments",
        url_prefix="/data",
        results_per_page=100,
    )
    dem_blueprint = manager.create_api(
        Demographics,
        collection_name="demographics",
        url_prefix="/data",
        results_per_page=100,
    )
    soc_blueprint = manager.create_api(
        SocialServices,
        collection_name="socialservices",
        url_prefix="/data",
        results_per_page=100,
    )
    # Special endpoint to make tracking unit tests easier for front-end
    uni_blueprint = manager.create_api(
        UnitTests,
        collection_name="unittests",
        url_prefix="/data",
        results_per_page=10,
    )
