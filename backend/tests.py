import unittest
from models import *
from schemas import *
from api import cap_format

# This file does basic unit testing of our backend code using Python's
# unittest framework.


class TestMethods(unittest.TestCase):
    # Tests if the query formatter handles cases correctly
    # and can deal with multiple worded places and places
    # with special characters
    def test_Cap_Format_1(self):
        test = cap_format("mobile")
        correct = "Mobile"
        self.assertEqual(str(test), correct)

    def test_Cap_Format_2(self):
        test = cap_format("cAliFoRnIa")
        correct = "California"
        self.assertEqual(str(test), correct)

    def test_Cap_Format_3(self):
        test = cap_format("los angeles")
        correct = "Los Angeles"
        self.assertEqual(str(test), correct)

    def test_Cap_Format4(self):
        test = cap_format("O v eR lY c 0 m pL 3x!")
        correct = "O V Er Ly C 0 M Pl 3x!"
        self.assertEqual(str(test), correct)

    def test_Cap_Format_5(self):
        test = cap_format("ike's point")
        correct = """Ike's Point"""
        self.assertEqual(str(test), correct)

    # Tests if the schemas are able to serialize the models passed in to
    # JSON. If it fails, it will return a very short response.
    def test_DepartmentSchema_1(self):
        dep = Departments(
            "a", "testname", "a", "a", 1, 1, 1, 1, 1.1, 1, 1, 1, 1, 1, 1, 1,
            "a", 1.1
        )
        schema = DepartmentSchema()
        longEnough = 250
        self.assertGreater(len(str(schema.dump(dep))), longEnough)

    def test_DemographicSchema_2(self):
        dem = Demographics(1,2,3,4,5,6,'testcity',8,9,10,11,12,13,14,15,16,
            17,2,2,2,2,2,2,2,2,2,2,2,2,'a',3,3,3,3,3,3,'a')
        schema = DemographicSchema()
        longEnough = 300
        self.assertGreater(len(str(schema.dump(dem))), longEnough)

    def test_SocialServiceSchema_3(self):
        dep = SocialServices(
            "a", "teststate", 1, 1, 1.2, 1, 1, 1.2, 1, 1, 1.2, "a"
        )
        schema = SocialServiceSchema()
        longEnough = 200
        self.assertGreater(len(str(schema.dump(dep))), longEnough)

    # Tests if ordering is showing as on or off for each schema, which
    # is important to how some data is presented.
    def test_DemographicOrdered_1(self):
        schema = DemographicSchema()
        try:
            ordered = schema.Meta.ordered
        except:
            ordered = False
        self.assertTrue(ordered)

    def test_DepartmentOrdered_2(self):
        schema = DepartmentSchema()
        try:
            ordered = schema.Meta.ordered
        except:
            ordered = False
        self.assertFalse(ordered)

    def test_SocialServiceOrdered_3(self):
        schema = SocialServiceSchema()
        try:
            ordered = schema.Meta.ordered
        except:
            ordered = False
        self.assertFalse(ordered)

    # Tests if the model objects return the correct string representation.
    def test_UnitTestsModel_1(self):
        test = UnitTests(5, 1, 1, 1, 1)
        correct = "<Total 5>"
        self.assertEqual(str(test), correct)

    def test_DepartmentsModel_2(self):
        test = Departments(
            "a", "testname", "a", "a", 1, 1, 1, 1, 1.1, 1, 1, 1, 1, 1, 1, 1,
            "a", 1.1
        )
        correct = "<Name 'testname'>"
        self.assertEqual(str(test), correct)

    def test_DemographicsModel_3(self):
        test = Demographics(1,2,3,4,5,6,'testcity',8,9,10,11,12,13,14,15,16,
            17,2,2,2,2,2,2,2,2,2,2,2,2,'a',3,3,3,3,3,3,'a')
        correct = "<City 'testcity'>"
        self.assertEqual(str(test), correct)

    def test_SocialServicesModel_4(self):
        test = SocialServices(
            "a", "teststate", 1, 1, 1.2, 1, 1, 1.2, 1, 1, 1.2, "a"
        )
        correct = "<State 'teststate'>"
        self.assertEqual(str(test), correct)


if __name__ == "__main__":
    unittest.main()
