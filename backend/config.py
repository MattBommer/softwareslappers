# This stores the configuration for the Flask application.

class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = '<SECRET KEY>'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = '<DATABASE URI>'

class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
