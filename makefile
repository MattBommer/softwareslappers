#use this to open the webpage in browser (for linux or OS with linux kernel)
webpage:
	open www/index.html

# Use this to open the webpage in browser (for windows)
webpage_wind:
	start www/index.html

# Use this to package the backend folder for deployment to Elastic Beanstalk
backendsource:
	rm -f ./backend/backendsource.zip
	rm -f ./backend/*.pyc
	rm -f ./backend/requirements.txt
	cp ./requirements.txt ./backend/
	zip -j ./backend/backendsource.zip ./backend/*
	rm -f ./backend/requirements.txt

# Use this command to build the copwatch:backend docker image from the dockerfile
buildb:
	cp requirements.txt ./docker
	docker build ./docker -f docker/Backend -t copwatch:backend
	rm docker/requirements.txt

# Use this command to run the backend docker image
runb:
	docker run -it -v "$(PWD)":/usr/copwatch copwatch:backend

# Use this command to build the copwatch:frontend docker image from the dockerfile
buildf:
	cp frontend/package.json ./docker
	cp frontend/yarn.lock ./docker
	mkdir docker/frontend_cop
	rsync -av frontend/ docker/frontend_cop --exclude=node_modules
	docker build ./docker -f docker/Frontend -t copwatch:frontend
	rm docker/package.json
	rm docker/yarn.lock
	rm -rf docker/frontend_cop

# Use this command to run the docker image for frontend (note this won't pull up the browser automatically so go to localhost:3000 to see the dev site)
runf:
	docker run -it -p 3000:3000 copwatch:frontend
