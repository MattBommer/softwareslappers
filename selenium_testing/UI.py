from selenium import webdriver
import unittest
import os

driver = webdriver.Chrome(executable_path="chromedriver.exe")
driver.set_window_size(1080, 800)

class UITests(unittest.TestCase) :
    
    def test_home(self) :
        driver.get("https://copwatchusa.com/")
        homeButton = driver.find_element_by_xpath("//*[@id=\"Dynamic NavBar\"]/div/a[1]")
        homeButton.click()

    def test_police(self) :
        driver.get("https://copwatchusa.com/")
        homeButton = driver.find_element_by_xpath("//*[@id=\"Dynamic NavBar\"]/div/a[2]")
        homeButton.click()
        self.assertEquals(driver.current_url, "https://copwatchusa.com/PoliceDepartments")

    def test_demos(self) :
        driver.get("https://copwatchusa.com/")
        demoButton = driver.find_element_by_xpath("//*[@id=\"Dynamic NavBar\"]/div/a[3]")
        demoButton.click()
        self.assertEquals(driver.current_url, "https://copwatchusa.com/Demographics")

    def test_social(self) :
        driver.get("https://copwatchusa.com/")
        socialButton = driver.find_element_by_xpath("//*[@id=\"Dynamic NavBar\"]/div/a[4]")
        socialButton.click()
        self.assertEquals(driver.current_url, "https://copwatchusa.com/SocialServices")

    def test_about(self) :
        driver.get("https://copwatchusa.com/")
        aboutButton = driver.find_element_by_xpath("//*[@id=\"Dynamic NavBar\"]/div/a[5]")
        aboutButton.click()
        self.assertEquals(driver.current_url, "https://copwatchusa.com/About")

    def cleanup(self) :
        driver.quit()


if __name__ == "__main__" :
    unittest.main()